<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;

class PassportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->truncate();
        Artisan::call('passport:client', [
            '--password' => true, '--name' => 'Main Password Grant'
        ]);
        DB::table('oauth_clients')->where('id', 1)->update([
            'secret' => 'Ckc8R8egR4haz92lfUsKRdMSqgp3NHWnHWnAwM34',
            'redirect' => null,
        ]);
    }
}
