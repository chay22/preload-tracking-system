<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        User::unguard();
        $this->seedUsers();
        User::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    protected function seedUsers()
    {
        User::truncate();

        foreach ([
            [
                'username' => 'admin',
                'password' => Hash::make('admin'),
                'role_id' => 1,
            ]
        ] as $user) {
            User::create($user);
        }
    }
}
