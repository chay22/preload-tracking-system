<?php

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->seedRoles();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }

    protected function seedRoles()
    {
        Role::truncate();

        $groups = [
            [
                'key' => 'admin',
                'name' => 'Admin',
            ],
            [
                'key' => 'user',
                'name' => 'User',
            ],
        ];

        $admin = null;

        foreach ($groups as $value) {
            $group = new Role($value);
            $group->key = $value['key'];
            $group->save();
        }
    }
}
