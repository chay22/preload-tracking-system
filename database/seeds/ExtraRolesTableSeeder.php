<?php

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExtraRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->seedRoles();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }

    protected function seedRoles()
    {
        $groups = [
            [
                'key' => 'assistant',
                'name' => 'Assistant',
            ],
        ];

        $admin = null;

        foreach ($groups as $value) {
            if (! Role::find($value['key'])) {
                $group = new Role($value);
                $group->key = $value['key'];
                $group->save();
            }
        }
    }
}
