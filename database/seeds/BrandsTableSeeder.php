<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;
use App\Models\BrandType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Brand::unguard();
        BrandType::unguard();
        $this->seedBrands();
        BrandType::reguard();
        Brand::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    protected function seedBrands()
    {
        Brand::truncate();
        BrandType::truncate();

        $brandData = [
            'OPPO' => [
                'A5S',
                'A1K',
                'A5S',
                'A5',
                'A9',
                'RENO 2F',
                'RENO 2',
            ],

            'INFINIX' => [
                'A5S',
                'A1K',
                'A5S',
                'A5',
                'A9',
                'RENO 2F',
                'RENO 2',
            ],

            'XIAOMI' => [
                'REDMI NOTE 8',
                'REDMI NOTE 8 PRO',
                'REDMI 8A',
                'REDMI NOTE 5A',
                'REDMI 6A ',
                'REDMI NOTE 5',
                'REDMI 5A',
                'REDMI 8',
                'REDMI 5',
                'REDMI 4X',
                'REDMI 4A',
                'MI A1',
                'REDMI NOTE 9 PRO',
                'REDMI 5+',
                'REDMI NOTE 7',
                'REDMI 6',
                'REDMI 3',
                'REDMI NOTE 2',
                'REDMI K20 PRO',
                'MI NOTE 10 (CC 9 PRO)',
                'REDMI 7',
                'REDMI NOTE 4',
                'MI A2',
                'POCOPHONE F1',
                'MI MIX 3',
                'REDMI 6 PRO ',
                'REDMI 3S',
                'REDMI 2',
                'REDMI NOTE 7 PRO',
                'REDMI 6 NOTE PRO',
                'POCOPHONE X2',
                'REDMI NOTE 4X',
                'MI 8 PRO',
                'REDMI K30',
                'MI MIX 4',
                'MI MAX 3',
                'MI 5',
                'REDMI K20 PRO',
                'MI 9 PRO 5G',
                'MI 9T PRO',
                'MI 6X',
                'MI A2 LITE',
                'MI MIX',
                'BLACK SHARK GAMING PHONE',
                'REDMI 3X',
                'REDMI NOTE 8T',
                'MI NOTE BAMBOO',
                'MI 9T',
                'REDMI 7A',
                'MI PAD 4',
                'MI 10 PRO',
                'MI 8 LITE',
                'REDMI S2',
                'REDMI 8A PRO',
                'NOTE 9 PRO MAX',
                'MI 5S',
                'REDMI GO',
                'MI 9 SE',
                'MI NOTE 2',
                'MI 8',
                'MI 6',
                'MI 9',
                'MI 4S',
                'REDMI NOTE 3',
                'REDMI PRO 2',
                'MI 8 SE',
                'REDMI NOTE 3',
                'MI PAD 4+',
                'MI MIX 3',
            ],

            'LAVA' => [
                'REDMI NOTE 8',
                'REDMI NOTE 8 PRO',
                'REDMI 8A',
                'REDMI NOTE 5A',
                'REDMI 6A ',
                'REDMI NOTE 5',
                'REDMI 5A',
                'REDMI 8',
                'REDMI 5',
                'REDMI 4X',
                'REDMI 4A',
                'MI A1',
                'REDMI NOTE 9 PRO',
                'REDMI 5+',
                'REDMI NOTE 7',
                'REDMI 6',
                'REDMI 3',
                'REDMI NOTE 2',
                'REDMI K20 PRO',
                'MI NOTE 10 (CC 9 PRO)',
                'REDMI 7',
                'REDMI NOTE 4',
                'MI A2',
                'POCOPHONE F1',
                'MI MIX 3',
                'REDMI 6 PRO ',
                'REDMI 3S',
                'REDMI 2',
                'REDMI NOTE 7 PRO',
                'REDMI 6 NOTE PRO',
                'POCOPHONE X2',
                'REDMI NOTE 4X',
                'MI 8 PRO',
                'REDMI K30',
                'MI MIX 4',
                'MI MAX 3',
                'MI 5',
                'REDMI K20 PRO',
                'MI 9 PRO 5G',
                'MI 9T PRO',
                'MI 6X',
                'MI A2 LITE',
                'MI MIX',
                'BLACK SHARK GAMING PHONE',
                'REDMI 3X',
                'REDMI NOTE 8T',
                'MI NOTE BAMBOO',
                'MI 9T',
                'REDMI 7A',
                'MI PAD 4',
                'MI 10 PRO',
                'MI 8 LITE',
                'REDMI S2',
                'REDMI 8A PRO',
                'NOTE 9 PRO MAX',
                'MI 5S',
                'REDMI GO',
                'MI 9 SE',
                'MI NOTE 2',
                'MI 8',
                'MI 6',
                'MI 9',
                'MI 4S',
                'REDMI NOTE 3',
                'REDMI PRO 2',
                'MI 8 SE',
                'REDMI NOTE 3',
                'MI PAD 4+',
                'MI MIX 3',
            ],


            'REALME' => [
                '5 PRO',
                '5i',
                'C3',
                'C2',
                'XT',
                '6 Pro',
                '6',

            ],

            'VIVO' => [
                'V19',
                'Y19',
                'Y17',
                'Y15',
                'Y12',
                'Y91C',
            ],

            'SAMSUNG' => [
                'GALAXY A10',
                'GALAXY A20S',
                'GALAXY A30S',
                'GALAXY AM30S',
                'GALAXY A51',
                'GALAXY A10S',
                'GALAXY A71',
                'GALAXY A50S',
                'GALAXY M31',
                'GALAXY S8',
                'GALAXY J2 PRIME',
                'GALAXY NOTE 8',
                'GALAXY A20',
                'GALAXY S8+',
                'GALAXY V PLUS',
                'GALAXY A6',
                'GALAXY S9',
                'GALAXY J4+',
                'GALAXY J6',
                'GALAXY J5',
                'GALAXY NOTE 9',
                'GALAXY CORE 2',
                'GALAXY A01',
                'GALAXY A5',
                'GALAXY A7',
                'GALAXY S20',
                'GALAXY A70',
                'GALAXY M20',
                'GALAXY J6+',
                'GALAXY A50',
                'GALAXY J7',
                'GALAXY J4',
                'GALAXY A2 CORE',
            ],
        ];


        foreach ($brandData as $brand => $brandTypes) {
            $brand = [
                'name' => $brand,
            ];

            $brand = Brand::create($brand);
            usleep(200);

            foreach ($brandTypes as $brandType) {
                $brand->types()->create([
                    'name' => $brandType,
                ]);

                usleep(100);
            }
        }
    }
}
