<?php

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->seedPermission();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }

    protected function seedPermission()
    {
        DB::table('role_permission')->truncate();
        Permission::flushCached();
        Permission::truncate();

        $permissions = [
            'Dashboard' => [
                [
                    'key' => 'access.dashboard',
                    'name' => 'Access (login to) dashboard',
                ],
            ],
            'Assignment' => [
                [
                    'key' => 'access.assignment',
                    'name' => 'Access assignment page',
                ],
                [
                    'key' => 'export.assignment',
                    'name' => 'Export assignments',
                ],
            ],
            'Tool' => [
                [
                    'key' => 'access.tool',
                    'name' => 'Access tool page',
                ],
                [
                    'key' => 'create.tool',
                    'name' => 'Create new tool',
                ],
                [
                    'key' => 'edit.tool',
                    'name' => 'Edit tool',
                ],
                [
                    'key' => 'delete.tool',
                    'name' => 'Delete tool',
                ],
            ],
            'Dealer' => [
                [
                    'key' => 'access.dealer',
                    'name' => 'Access dealer page',
                ],
                [
                    'key' => 'create.dealer',
                    'name' => 'Create new dealer',
                ],
                [
                    'key' => 'edit.dealer',
                    'name' => 'Edit dealer',
                ],
                [
                    'key' => 'delete.dealer',
                    'name' => 'Delete dealer',
                ],
            ],
            'Brand' => [
                [
                    'key' => 'access.brand',
                    'name' => 'Access brand page',
                ],
                [
                    'key' => 'create.brand',
                    'name' => 'Create new brand',
                ],
                [
                    'key' => 'edit.brand',
                    'name' => 'Edit brand',
                ],
                [
                    'key' => 'delete.brand',
                    'name' => 'Delete brand',
                ],
                [
                    'key' => 'access.brand_type',
                    'name' => 'Manage brand type',
                ],
                [
                    'key' => 'create.brand_type',
                    'name' => 'Create new brand type',
                ],
                [
                    'key' => 'edit.brand_type',
                    'name' => 'Edit brand type',
                ],
                [
                    'key' => 'delete.brand_type',
                    'name' => 'Delete brand type',
                ],
            ],
            'Location' => [
                [
                    'key' => 'access.location',
                    'name' => 'Access location page',
                ],
                [
                    'key' => 'create.location',
                    'name' => 'Create new location',
                ],
                [
                    'key' => 'edit.location',
                    'name' => 'Edit location',
                ],
                [
                    'key' => 'delete.location',
                    'name' => 'Delete location',
                ],
            ],
            'Users' => [
                [
                    'key' => 'access.users',
                    'name' => 'Access user management page',
                ],
                [
                    'key' => 'create.users',
                    'name' => 'Create new user',
                ],
                [
                    'key' => 'edit.users',
                    'name' => 'Edit users',
                ],
                [
                    'key' => 'delete.users',
                    'name' => 'Delete users',
                ],
                [
                    'key' => 'create.role',
                    'name' => 'Create new role',
                ],
                [
                    'key' => 'edit.role',
                    'name' => 'Edit role',
                ],
                [
                    'key' => 'delete.role',
                    'name' => 'Delete role',
                ],
                [
                    'key' => 'access.permission',
                    'name' => 'Access role permission',
                ],
                [
                    'key' => 'edit.permission',
                    'name' => 'Edit role permission',
                ],
            ],
            'Setting' => [
                [
                    'key' => 'access.setting',
                    'name' => 'Access setting page',
                ],
                [
                    'key' => 'edit.setting',
                    'name' => 'Edit setting',
                ],
            ],
        ];

        $data = new Collection();

        foreach ($permissions as $module => $perms) {
            foreach ($perms as $perm) {
                $data[] = Permission::create(array_merge(['module' => $module], $perm));
            }
        }

        Role::where('key', 'admin')->first()->permissions()->sync($data->pluck('id'));
    }
}
