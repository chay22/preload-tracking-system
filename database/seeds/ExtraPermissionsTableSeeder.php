<?php

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ExtraPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->seedPermission();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }

    protected function seedPermission()
    {
        Permission::flushCached();

        $permissions = [
            'Location' => [
                [
                    'key' => 'access.location',
                    'name' => 'Access location page',
                ],
                [
                    'key' => 'create.location',
                    'name' => 'Create new location',
                ],
                [
                    'key' => 'edit.location',
                    'name' => 'Edit location',
                ],
                [
                    'key' => 'delete.location',
                    'name' => 'Delete location',
                ],
            ],
        ];

        $data = new Collection();

        foreach ($permissions as $module => $perms) {
            foreach ($perms as $perm) {
                $data[] = Permission::firstOrCreate(['key' => $perm['key']], array_merge(['module' => $module], $perm));
            }
        }

        Role::where('key', 'admin')->first()->permissions()->syncWithoutDetaching(Permission::pluck('id'));
    }
}
