<?php

use Illuminate\Database\Seeder;
use App\Models\Dealer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DealersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Dealer::unguard();
        $this->seedDealers();
        Dealer::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    protected function seedDealers()
    {
        Dealer::truncate();

        $dealerData = [
            '3 Pendekar',
            '36 Shop',
            '88 Jaya',
            'Abdul Fatah Cell',
            'Agung Cell',
            'Akar Daya',
            'Alibaba Store',
            'Aneka',
            'ASK Cimahi',
            'Atlantik Bandung',
            'Atlantik Cell',
            'Awong Cell',
            'B-Phone',
            'BBS',
            'Bintang Timur Sellindo',
            'Bonafide',
            'BRP',
            'Bumilindo',
            'Caesar',
            'Central Gadget Store',
            'Channel Cellular',
            'Choco Cell',
            'Complete Selular',
            'Devi Cell',
            'Dunia Cell',
            'Duta Cell',
            'Duta Cell',
            'E-Mobile',
            'Energy',
            'Eropa Cell',
            'G2 Cell',
            'Gadget Cell',
            'Galeria',
            'GCC',
            'Geria',
            'GH Shop',
            'Global Dunia Technology',
            'Goeboek Imoet',
            'Gracia',
            'Graha Cellular',
            'Grand Cell',
            'Gudang Cell',
            'Gumilar',
            'HDK Cell',
            'Hokage',
            'Iluva',
            'Ivan Cell',
            'Jaya Phone',
            'JK Phoneshop',
            'King Cell (QQ)',
            'MBC',
            'Meteor',
            'MGM',
            'Michael HP',
            'MJK Cell',
            'Naga Mas 88 Telecom',
            'Olivia',
            'Omega',
            'One Gemilang (WSJ)',
            'Option Cell',
            'Orbit',
            'Pegasus',
            'Phone Center',
            'PMMN - Telesindo',
            'Polindo',
            'Presiden Sellular',
            'Pribadi Cell',
            'Queen Cell',
            'Rajawali Cell',
            'Raya Cellular',
            'Restu Mama',
            'Selular Senjaya',
            'Sentra Ponsel',
            'Seven',
            'Sidoarjo',
            'Sinar Jaya',
            'Sinar Mas Semarang',
            'Sinar Shop',
            'Sinergi 168 Solo',
            'SMS Cellular',
            'SSM Bandung',
            'Sunjaya',
            'SWT Cell',
            'T-Cell',
            'Tankindo (PDS)',
            'Tele Ring',
            'Telesindo',
            'Telolet Cell',
            'Terminal Cell',
            'Tiga Putra',
            'Tops Cell',
            'Unicell',
            'Unistar',
            'Visitel',
            'Vivi Cell',
            'W-Shop',
        ];



        foreach ($dealerData as $dealer) {
            $dealer = [
                'name' => $dealer,
            ];

            Dealer::create($dealer);
            usleep(200);
        }
    }
}
