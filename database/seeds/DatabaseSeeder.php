<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PassportsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(DealersTableSeeder::class);
        $this->call(ToolsTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
    }
}
