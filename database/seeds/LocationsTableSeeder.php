<?php

use App\Models\Location;
use App\Models\Permission;
use App\Models\Dealer;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Location::unguard();
        $this->seedLocations();
        Location::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    protected function seedLocations()
    {
        Location::truncate();

        $data = [
            [
                'name' => 'Jakarta',
            ],
            [
                'name' => 'Bandung',
            ]
        ];

        $locations = new Collection();

        foreach ($data as $location) {
            $locations[] = Location::create($location);
        }

        foreach (Dealer::all() as $dealer) {
            $dealer->locations()->sync([1]);
        }

        foreach (User::all() as $user) {
            $user->locations()->sync([1]);
        }
    }

}
