<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    DB,
    Redirect,
    Session
};
use App\Models\Assignment;
use App\Models\AssignmentInput;
use App\Models\User;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // if (Auth::check()) {
        //     $user = Auth::user();
        //     $group = $user->group;

        //     if ($group) {
        //         if ($group->key === 'admin') {
        //             return Redirect::route('antrean.index');
        //         } else if ($group->key === 'operator') {
        //             return Redirect::route('antrean.index');
        //         } else if ($group->key === 'monitor') {
        //             return Redirect::route('monitor.index');
        //         } else {

        //         }
        //     }
        // }
        $totalUsers = User::count();
        $totalAssignments = Assignment::count();
        $totalInputs = AssignmentInput::count();

        return Inertia::render('Dashboard/Index', [
            'totalUsers' => $totalUsers,
            'totalAssignments' => $totalAssignments,
            'totalInputs' => $totalInputs,
            'assignmentLast7Days' => [],
            'assignmentByAssignees' => new \stdClass(),
        ]);
    }

    protected function getAssignmentLast7Days()
    {
        $assignmentLast7Days = DB::table('assignments')
                            ->selectRaw('COUNT(assignments.id) total, assignments.dealer_id, dealers.name, assignments.assigned_at')
                            ->join('dealers', 'assignments.dealer_id', '=', 'dealers.id')
                            ->whereRaw('assignments.assigned_at >= DATE(NOW()) - INTERVAL 7 DAY')
                            ->whereRaw('assignments.deleted_at IS NULL')
                            ->groupBy('assignments.dealer_id')
                            ->get();

        $last7Days = [];

        for ($i = 6; $i >= 0; $i--) {
            $last7Days[Carbon::now()->startOfDay()->subtract($i, 'days')->format('Y-m-d H:i:s')] = [];
        }

        $greatestTotalInDay = 0;

        foreach ($assignmentLast7Days as $assignment) {
            if (isset($last7Days[$assignment->assigned_at]) && ! isset($last7Days[$assignment->assigned_at][$assignment->dealer_id])) {
                $last7Days[$assignment->assigned_at][$assignment->dealer_id] = (array) $assignment;
            }

            $total = count($last7Days[$assignment->assigned_at]);

            if ($total > $greatestTotalInDay) {
                $greatestTotalInDay = $total;
            }
        }


        $assignmentLast7Days = [];

        foreach ($last7Days as $assignments) {
            $assignmentLast7Days[] = array_values($assignments);
        }

        if ($greatestTotalInDay) {
            $last7Days = [];
            $greatestTotalInDay -= 1;


            foreach (range(0, $greatestTotalInDay) as $i) {
                $last7Days[$i] = ['name' => '', 'data'=>[0, 0, 0, 0, 0, 0, 0]];

                foreach ($assignmentLast7Days as $key => $assignments) {
                    if (isset($assignments[$i])) {
                        $last7Days[$i]['id'] = $assignments[$i]['name'];
                        $last7Days[$i]['data'][$key] = $assignments[$i]['total'];
                    }
                }
            }

            $assignmentLast7Days = $last7Days;
        }

        return $assignmentLast7Days;
    }

    protected function getAssignmentByAssignees()
    {
        $assignments = DB::table('assignments')
                            ->selectRaw('COUNT(assignments.id) total, assignments.created_by, IF(creator.name IS NULL, creator.username, creator.name) as display_name')
                            ->join('users as creator', 'assignments.created_by', '=', 'creator.id')
                            ->whereRaw('assignments.created_by IS NOT NULL')
                            ->whereRaw('assignments.deleted_at IS NULL')
                            ->groupBy('assignments.created_by')
                            ->get();

        $assigneeAssignments = [];

        foreach ($assignments as $assignment) {
            if (! isset($assigneeAssignments[$assignment->display_name])) {
                $assigneeAssignments[$assignment->display_name] = $assignment->total;
            }
        }

        $assignments = [
            'assignees' => [],
            'data' => [],
        ];

        foreach ($assigneeAssignments as $display_name => $total) {
            $assignments['assignees'][] = $display_name;
            $assignments['data'][] = $total;
        }

        return $assignments;
    }
}
