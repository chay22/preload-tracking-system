<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\Assignment;
use App\Models\Dealer;
use App\Models\Tool;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AssignmentsExport;
use App\Exports\SummaryExport;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('access.assignment');

        $result = $this->queryAssignment($request);

        $summaries = $result['assignments'];
        $pageFilters = $result['pageFilters'];
        $pageSort = $result['pageSort'];
        $pageLimit = $result['pageLimit'];
        $pageOffset = $result['pageOffset'];
        $totalRecords = $result['totalRecords'];

        $assignments = [];
        $users = [];
        $i = 1;

        foreach ($summaries as $item) {
            $lastModifiedAt = ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i');

            $assignment = [
                'id' => $item->id,
                'assigned_at' => Carbon::parse($item->assigned_at)->format('d-m-Y'),
                'dealer' => $item->dealer,
                'tool' => $item->tool,
                'owner' => $item->owner,
                'status' => $item->status,
                'total_success' => $item->inputs->sum->success_count,
                'total_failed' => $item->inputs->sum->failed_count,
                'submitted_at' => $item->status ? $lastModifiedAt : $item->created_at->format('d-m-Y H:i'),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
            ];

            foreach ($item->users as $user) {
                $assignments[] = array_merge($assignment, [
                    'id_' => $item->id . '-' . $user->id,
                    'id' => $i,
                    'name' => $user->name,
                    'display_name' => $user->display_name,
                    'username' => $user->username,
                    'count' => $user->count,
                    'type' => $user->type,
                ]);

                if (! isset($users[$user->username])) {
                    $users[$user->username] = [
                        'id' => $user->id,
                        'display_name' => $user->display_name,
                        'username' => $user->username,
                        'total' => $user->count,
                    ];
                } else {
                    $users[$user->username]['total'] += $user->count;
                }

                $i++;
            }
        }

        ksort($users);

        $totalRecords = count($assignments);

        return Inertia::render('Summary/Index', [
            'can' => [
                'create.assignment' => Auth::user()->can('create.assignment'),
                'edit.assignment' => Auth::user()->can('edit.assignment'),
                'delete.assignment' => Auth::user()->can('delete.assignment'),
                'export.assignment' => Auth::user()->can('export.assignment'),
            ],
            'assignments' => $assignments,
            'tools' => Tool::where('status', 1)->get()->map(function ($item) {
                return [
                    'id' => $item->id,
                    'tool_id' => $item->id,
                ];
            }),
            'dealers' => Dealer::where('status', 1)->get()->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            }),
            'users' => array_values($users),
            'page_filters' => $pageFilters,
            'page_sort' =>  $pageSort,
            'page_limit' => (int) $pageLimit,
            'page_offset' => (int) $pageOffset,
            'total_records' => (int) $totalRecords,
        ]);
    }

    protected function queryAssignment(Request $request)
    {

        $pageFilters = null;
        $pageSort = null;
        $pageLimit = 25;
        $pageOffset = 0;
        $totalRecords = 0;
        $assignments = [];
        $query = Assignment::with(['inputs', 'assistants', 'owner']);

        if ($request->has('filter') && $request->filled('filter')) {
            $pageFilters = $request->filter;

            if (isset($pageFilters['global'])) {
                $query->where('name', 'like', '%'.$pageFilters['global'].'%');
            }

            if (isset(($pageFilters['status']))) {
                $pageFilters['status'] = (int) $pageFilters['status'];
                $query->where('status', $pageFilters['status']);
            }

            if (isset($pageFilters['month'])) {
                $query->whereMonth('assigned_at', Carbon::parse($pageFilters['month']));
            } else {
                return compact('assignments', 'totalRecords', 'pageFilters', 'pageSort', 'pageLimit', 'pageOffset');
            }

            if (isset($pageFilters['dealer'])) {
                $pageFilters['dealer'] = (int) $pageFilters['dealer'];
                $query->where('dealer_id', $pageFilters['dealer']);
            }

            if (isset($pageFilters['tool'])) {
                $pageFilters['tool'] = $pageFilters['tool'];
                $query->where('tool_id', $pageFilters['tool']);
            }
        } else {
            return compact('assignments', 'totalRecords', 'pageFilters', 'pageSort', 'pageLimit', 'pageOffset');
        }

        if ($request->filled('sort')) {
            $pageSort = $request->sort;

            if (isset($pageSort['by'], $pageSort['dir'])) {
                $pageSort['dir'] = (int) $pageSort['dir'];

                if (strpos($pageSort['by'], '.') !== false) {
                    $relations = explode('.', $pageSort['by']);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;
                    $column = $relations[$lastIndex];
                    unset($relations[$lastIndex]);
                    $relations = implode('.', $relations);

                    $query->whereHas($relations, function ($q) use ($column, $pageSort) {
                        $q->orderBy($column, $pageSort['dir'] < 0 ? 'desc' : 'asc');
                    });
                } else {
                    $query->orderBy($pageSort['by'], $pageSort['dir'] < 0 ? 'desc' : 'asc');
                }
            }
        } else {
            $pageSort = [
                'by' => 'id',
                'dir' => 1,
            ];
            $query->orderBy('id', 'DESC');
        }

        if ($request->filled('limit')) {
            $pageLimit = $request->limit;
        }

        if ($request->filled('offset')) {
            $pageOffset = $request->offset;
        }

        $assignments = $query->get();


        $totalRecords = count($assignments);

        return compact('assignments', 'totalRecords', 'pageFilters', 'pageSort', 'pageLimit', 'pageOffset');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $assignments = [];

        if ($request->assignmentId) {
            $assignments = [Assignment::with(['inputs.type' => function ($q) {
                $q->orderByDesc($q->getModel()->getTable().'.id');
            }])->with('dealer', 'tool', 'photos', 'owner')->findOrFail($request->assignmentId)];
        } else {
            $result = $this->queryAssignment($request);
            $summaries = $result['assignments'] ?: [];
            $assignments = [];
            $users = [];
            $i = 0;

            foreach ($summaries as $item) {
                $lastModifiedAt = ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i');

                $assignment = [
                    'id' => $item->id,
                    'assigned_at' => Carbon::parse($item->assigned_at)->format('d-m-Y'),
                    'dealer' => $item->dealer,
                    'tool' => $item->tool,
                    'owner' => $item->owner,
                    'status' => $item->status,
                    'total_success' => $item->inputs->sum->success_count,
                    'total_failed' => $item->inputs->sum->failed_count,
                    'submitted_at' => $item->status ? $lastModifiedAt : $item->created_at->format('d-m-Y H:i'),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                ];

                foreach ($item->users as $user) {
                    $assignments[] = array_merge($assignment, [
                        'id_' => $item->id . '-' . $user->id,
                        'id' => $i,
                        'name' => $user->name,
                        'display_name' => $user->display_name,
                        'username' => $user->username,
                        'count' => $user->count,
                        'type' => $user->type,
                    ]);

                    if (! isset($users[$user->username])) {
                        $users[$user->username] = [
                            'id' => $user->id,
                            'display_name' => $user->display_name,
                            'username' => $user->username,
                            'total' => $user->count,
                        ];
                    } else {
                        $users[$user->username]['total'] += $user->count;
                    }

                    $i++;
                }
            }

        }

        $filename = date('U').mt_rand(1, 100);

        return Excel::download(new SummaryExport($assignments, $users), 'summary-'.$filename.'.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('edit.summary');
        $request->validate([
            'name' => ['required'],
            'status' => ['required'],
        ]);

        $summary = Summary::create($request->all());

        Session::flash('success', 'Summary succesfully created');

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $summary = Summary::findOrFail($id);

        // return Inertia::render('Summary/Edit', [
        //     'summary' => $summary
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit.summary');

        $summary = Summary::findOrFail($id);

        $request->validate([
            'name' => ['required',],
            'status' => ['required'],
        ]);

        $summary->name = $request->name;
        $summary->status = $request->status;

        $summary->save();

        Session::flash('success', __('Summary successfully updated'));

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Summary::findOrFail($id)->delete();

        Session::flash('success', __('Summary successfully deleted'));

        return Redirect::back();
    }
}
