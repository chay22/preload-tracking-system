<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\Brand;
use App\Models\BrandType;

class BrandTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $brandId)
    {
        $this->authorize('access.brand');
        $this->authorize('access.brand_type');

        $brand = Brand::findOrFail($brandId);

        $pageFilters = null;
        $pageSort = null;
        $pageLimit = 25;
        $pageOffset = 0;
        $query = BrandType::where('brand_id', $brand->id);

        if ($request->has('filter') && $request->filled('filter')) {
            $pageFilters = $request->filter;

            if (isset($pageFilters['global'])) {
                $query->where('name', 'like', '%'.$pageFilters['global'].'%');
            }

            if (isset(($pageFilters['status']))) {
                $pageFilters['status'] = (int) $pageFilters['status'];
                $query->where('status', $pageFilters['status']);
            }
        }

        if ($request->filled('sort')) {
            $pageSort = $request->sort;

            if (isset($pageSort['by'], $pageSort['dir'])) {
                $pageSort['dir'] = (int) $pageSort['dir'];

                if (strpos($pageSort['by'], '.') !== false) {
                    $relations = explode('.', $pageSort['by']);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;
                    $column = $relations[$lastIndex];
                    unset($relations[$lastIndex]);
                    $relations = implode('.', $relations);

                    $query->whereHas($relations, function ($q) use ($column, $pageSort) {
                        $q->orderBy($column, $pageSort['dir'] < 0 ? 'desc' : 'asc');
                    });
                } else {
                    $query->orderBy($pageSort['by'], $pageSort['dir'] < 0 ? 'desc' : 'asc');
                }
            }
        } else {
            $pageSort = [
                'by' => 'id',
                'dir' => -1,
            ];
            $query->orderBy('id', 'DESC');
        }

        if ($request->filled('limit')) {
            $pageLimit = $request->limit;
        }

        if ($request->filled('offset')) {
            $pageOffset = $request->offset;
        }

        $brandTypeTypes = $query->get();
        $totalRecords = count($brandTypeTypes);

        return Inertia::render('BrandType/Index', [
            'can' => [
                'create.brand_type' => Auth::user()->can('create.brand_type'),
                'edit.brand_type' => Auth::user()->can('edit.brand_type'),
                'delete.brand_type' => Auth::user()->can('delete.brand_type'),
            ],
            'brand' => $brand,
            'brand_types' => $brandTypeTypes->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'status' => $item->status,
                    'active' => $item->active,
                    'last_modified_at' => ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i'),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                ];
            }),
            'page_filters' => $pageFilters,
            'page_sort' =>  $pageSort,
            'page_limit' => (int) $pageLimit,
            'page_offset' => (int) $pageOffset,
            'total_records' => (int) $totalRecords,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $brandId)
    {
        $this->authorize('edit.brand_type');

        $brand = Brand::findOrFail($brandId);

        $request->validate([
            'name' => ['required'],
            'status' => ['required'],
        ]);

        $brandType = BrandType::create(array_merge($request->all(), ['brand_id' => $brand->id]));

        Session::flash('success', 'Brand type succesfully created');

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $brandType = BrandType::findOrFail($id);

        // return Inertia::render('BrandType/Edit', [
        //     'brand' => $brandType
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $brandId, $id)
    {
        $this->authorize('edit.brand_type');

        $brand = Brand::findOrFail($brandId);
        $brandType = BrandType::findOrFail($id);

        $request->validate([
            'name' => ['required',],
            'status' => ['required'],
        ]);

        $brandType->name = $request->name;
        $brandType->status = $request->status;

        $brandType->save();

        Session::flash('success', __('Brand type successfully updated'));

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($brandId, $id)
    {
        $brand = Brand::findOrFail($brandId);

        BrandType::findOrFail($id)->delete();

        Session::flash('success', __('Brand type successfully deleted'));

        return Redirect::back();
    }
}
