<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\Location;
use App\Models\Dealer;
use App\Models\User;

class DealerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('access.dealer');

        $locations = Location::all();
        $users = User::whereHas('role', function ($q) {
            $q->where('roles.key', '!=', 'assistant');
        })->get();

        $pageFilters = null;
        $pageSort = null;
        $pageLimit = 25;
        $pageOffset = 0;
        $query = Dealer::with('locations');

        if ($request->has('filter') && $request->filled('filter')) {
            $pageFilters = $request->filter;

            if (isset($pageFilters['global'])) {
                $query->where('name', 'like', '%'.$pageFilters['global'].'%');
            }

            if (isset(($pageFilters['status']))) {
                $pageFilters['status'] = (int) $pageFilters['status'];
                $query->where('status', $pageFilters['status']);
            }

            if (isset($pageFilters['location'])) {
                $pageFilters['location'] = (int) $pageFilters['location'];
                $query->whereHas('locations', function ($q) use ($pageFilters) {
                    $q->where('location_id', $pageFilters['location']);
                });
            }
        }

        if ($request->filled('sort')) {
            $pageSort = $request->sort;

            if (isset($pageSort['by'], $pageSort['dir'])) {
                $pageSort['dir'] = (int) $pageSort['dir'];

                if (strpos($pageSort['by'], '.') !== false) {
                    $relations = explode('.', $pageSort['by']);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;
                    $column = $relations[$lastIndex];
                    unset($relations[$lastIndex]);
                    $relations = implode('.', $relations);

                    $query->whereHas($relations, function ($q) use ($column, $pageSort) {
                        $q->orderBy($column, $pageSort['dir'] < 0 ? 'desc' : 'asc');
                    });
                } else {
                    $query->orderBy($pageSort['by'], $pageSort['dir'] < 0 ? 'desc' : 'asc');
                }
            }
        } else {
            $pageSort = [
                'by' => 'id',
                'dir' => -1,
            ];
            $query->orderBy('id', 'DESC');
        }

        if ($request->filled('limit')) {
            $pageLimit = $request->limit;
        }

        if ($request->filled('offset')) {
            $pageOffset = $request->offset;
        }

        $dealers = $query->get();
        $totalRecords = count($dealers);

        return Inertia::render('Dealer/Index', [
            'can' => [
                'create.dealer' => Auth::user()->can('create.dealer'),
                'edit.dealer' => Auth::user()->can('edit.dealer'),
                'delete.dealer' => Auth::user()->can('delete.dealer'),
            ],
            'dealers' => $dealers->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'locations' => $item->locations->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'name' => $item->name,
                        ];
                    })->first(),
                    'users' => $item->users->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'username' => $item->username,
                        ];
                    }),
                    'status' => $item->status,
                    'active' => $item->active,
                    'last_modified_at' => ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i'),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                ];
            }),
            'locations' => $locations->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            }),
            'users' => $users->map(function ($item) {
                return [
                    'id' => $item->id,
                    'username' => $item->username,
                ];
            }),
            'page_filters' => $pageFilters,
            'page_sort' =>  $pageSort,
            'page_limit' => (int) $pageLimit,
            'page_offset' => (int) $pageOffset,
            'total_records' => (int) $totalRecords,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('edit.dealer');
        $request->validate([
            'name' => ['required'],
            'status' => ['required'],
        ]);

        $dealer = Dealer::create($request->all());

        if ($request->locations) {
            $dealer->locations()->sync($request->locations);
        }

        if ($request->users) {
            $dealer->users()->sync($request->users);
        }

        Session::flash('success', 'Dealer succesfully created');

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $dealer = Dealer::findOrFail($id);

        // return Inertia::render('Dealer/Edit', [
        //     'dealer' => $dealer
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit.dealer');

        $dealer = Dealer::findOrFail($id);

        $request->validate([
            'name' => ['required',],
            'status' => ['required'],
        ]);

        $dealer->name = $request->name;
        $dealer->status = $request->status;

        $dealer->save();

        if ($request->locations) {
            $dealer->locations()->sync($request->locations);
        }

        if ($request->users) {
            $dealer->users()->sync($request->users);
        }

        Session::flash('success', __('Dealer successfully updated'));

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dealer::findOrFail($id)->delete();

        Session::flash('success', __('Dealer successfully deleted'));

        return Redirect::back();
    }
}
