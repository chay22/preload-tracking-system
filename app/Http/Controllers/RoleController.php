<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('access.users');
        $this->authorize('create.role');

        $request->validate([
            'status' => ['required'],
            'name' => ['required', 'max:60', Rule::unique('roles', 'name')],
        ]);

        $role = Role::find($request->id) ?: new Role();
        $role->name = $request->name;
        $role->status = $request->status;

        $exists = $role->exists;

        $role->save();

        $message = __('Role successfully'.($exists ? 'updated' : 'created'));

        Session::flash('success', $message);

        return Redirect::route('users.index')->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $user = User::findOrFail($id);

        // return Inertia::render('Users/Edit', [
        //     'user' => [
        //         'id' => $user->id,
        //         'first_name' => $user->first_name,
        //         'last_name' => $user->last_name,
        //         'email' => $user->email,
        //         'owner' => $user->owner,
        //         'photo' => $user->photoUrl(['w' => 60, 'h' => 60, 'fit' => 'crop']),
        //         'deleted_at' => $user->deleted_at,
        //     ],
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('access.users');
        $this->authorize('edit.role');

        $role = Role::findOrFail($id);

        $request->validate([
            'status' => ['required'],
            'name' => ['required', 'max:60', Rule::unique('roles', 'name')->ignore($id)],
        ]);

        $role->name = $request->name;
        $role->status = $request->status;

        $role->save();

        $message = __('Role successfully updated');

        Session::flash('success', $message);

        return Redirect::route('users.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('access.users');
        $this->authorize('delete.role');

        $group = Role::findOrFail($id)->delete();

        Session::flash('success', __('Role successfully deleted'));

        return Redirect::route('users.index');
    }
}
