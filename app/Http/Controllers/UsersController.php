<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Role;
use App\Models\Location;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('access.users');
        $locations = Location::all();

        return Inertia::render('Users/Index', [
            'can' => [
                'create.users' => Auth::user()->can('create.users'),
                'edit.users' => Auth::user()->can('edit.users'),
                'delete.users' => Auth::user()->can('delete.users'),
                'create.role' => Auth::user()->can('create.role'),
                'edit.role' => Auth::user()->can('edit.role'),
                'delete.role' => Auth::user()->can('delete.role'),
                'access.permission' => Auth::user()->can('access.permission'),
                'edit.permission' => Auth::user()->can('edit.permission'),
            ],
            'users' => User::all()->map(function ($user) {
                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'username' => $user->username,
                    'role_id' => $user->role_id,
                    'role' => $user->role,
                    'locations' => $user->locations->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'name' => $item->name,
                        ];
                    })->first(),
                    'status' => $user->status,
                    'active' => $user->active,
                ];
            }),
            'roles' => Role::all()->map(function ($group) {
                return [
                    'id' => $group->id,
                    'key' => $group->key,
                    'name' => $group->name,
                    'status' => $group->status,
                    'active' => $group->active,
                ];
            }),
            'locations' => $locations->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            }),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('edit.users');
        $request->validate([
            'username' => ['required', Rule::unique('users', 'username')],
            'password' => ['required', 'confirmed', 'min:6'],
            'role_id' => ['required', Rule::exists('roles', 'id')],
            'status' => ['required'],
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);
        if ($request->locations) {
            $user->locations()->sync($request->locations);
        }

        Session::flash('success', 'User successfully created');

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $user = User::findOrFail($id);

        // return Inertia::render('Users/Edit', [
        //     'user' => [
        //         'id' => $user->id,
        //         'first_name' => $user->first_name,
        //         'last_name' => $user->last_name,
        //         'email' => $user->email,
        //         'owner' => $user->owner,
        //         'photo' => $user->photoUrl(['w' => 60, 'h' => 60, 'fit' => 'crop']),
        //         'deleted_at' => $user->deleted_at,
        //     ],
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit.users');

        $user = User::findOrFail($id);

        if ($request->password) {
            $request->validate([
                'password' => ['required', 'confirmed', 'min:6']
            ]);
            $user->password = Hash::make($request->password);
        } else {
            $request->validate([
                'username' => ['required', Rule::unique('users', 'username')->ignore($user->id)],
                'role_id' => ['required', Rule::exists('roles', 'id')],
                'status' => ['required'],
            ]);

            Role::findOrFail($request->role_id);

            $user->name = $request->name;
            $user->username = $request->username;
            $user->role_id = $request->role_id;
            $user->status = $request->status;

            if ($request->locations) {
                $user->locations()->sync($request->locations);
            }
        }

        $user->save();

        Session::flash('success', __('User successfully updated'));

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        Session::flash('success', __('User successfully deleted'));

        return Redirect::back();
    }
}
