<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    use Concerns\RequestQueryParamBuilder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searches = $this->getRequestSearch($request);
        $filters = $this->getRequestFilter($request);
        $limit = $this->getRequestLimit($request);
        $trashed = $this->getRequestTrashed($request);
        $sorts = $this->getRequestSort($request);
        $includes = $this->getRequestIncludes($request);
        $excludes = $this->getRequestExcludes($request);

        $q = User::with('locations');
        $model = $q->getModel();

        try {
            foreach ($searches as $search) {
                if (isset($search[3])) {
                    $q->where(function ($q) use ($search, $model) {
                        $field = 'name';
                        $operator = '=';

                        if (! empty($search[0])) {
                            $field = $search[0];
                        }

                        if (! empty($search[1])) {
                            $operator = $search[1];
                        }

                        $value = $search[2];

                        if (($field === 'name' || $field === 'username' || $field === 'display_name' || $field === 'role.name' || $field === 'location.name') && $operator === 'like' && is_string($value)) {
                            $value = '%'.$value.'%';
                        }

                        if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                            if (isset($search[5])) {
                                $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                    $table = $q->getModel()->getTable();

                                    if ($value === null) {
                                        if ($operator === '!=' || $operator === '<>') {
                                            $q->whereNotNull($table.'.'.$search[6]);
                                        } else {
                                            $q->whereNull($table.'.'.$search[6]);
                                        }
                                    } else {
                                        $q->where($table.'.'.$search[6], $operator, $value);
                                    }
                                });
                            } else {
                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        if ($field === 'display_name') {
                                            $q->whereNotNull('name');
                                        } else {
                                            $q->whereNotNull($field);
                                        }
                                    } else {
                                        if ($field === 'display_name') {
                                            $q->whereNull('name');
                                        } else {
                                            $q->whereNull($field);
                                        }
                                    }
                                } else {
                                    if ($field === 'display_name') {
                                        $q->where(function ($q) use ($operator, $value) {
                                            $q->where('name', $operator, $value)
                                                ->orWhere('username', $operator, $value);
                                        });
                                    } else {
                                        $q->where($field, $operator, $value);
                                    }
                                }
                            }
                        }

                        foreach ($search[3] as $s) {
                            $f = 'name';
                            $o = '=';

                            if (! empty($s[0])) {
                                $f = $s[0];
                            }

                            if (! empty($s[1])) {
                                $o = $s[1];
                            }

                            $v = $s[2];

                            if (($f === 'name' || $f === 'username' || $f === 'display_name' || $f === 'role.name' || $field === 'location.name') && $o === 'like' && is_string($v)) {
                                $v = '%'.$v.'%';
                            }

                            if ($f->isFillable($f) || $f == $model->getKeyName() || isset($s[5])) {
                                if (isset($s[5])) {
                                    $q->whereHas($s[5], function ($q) use ($s, $f, $o, $v) {
                                        $table = $q->getModel()->getTable();

                                        if ($v === null) {
                                            if ($o === '!=' || $o === '<>') {
                                                $q->whereNotNull($table.'.'.$s[6]);
                                            } else {
                                                $q->whereNull($table.'.'.$s[6]);
                                            }
                                        } else {
                                            $q->where($table.'.'.$s[6], $o, $v);
                                        }
                                    });
                                } else {
                                    if ($v === null) {
                                        if ($o === '!=' || $o === '<>') {
                                            if ($f === 'display_name') {
                                                $q->whereNotNull('name');
                                            } else {
                                                $q->whereNotNull($f);
                                            }
                                        } else {
                                            if ($f === 'display_name') {
                                                $q->whereNull('name');
                                            } else {
                                                $q->whereNull($f);
                                            }
                                        }
                                    } else {
                                        if ($f === 'display_name') {
                                            $q->where(function ($q) use ($o, $v) {
                                                $q->where('name', $o, $v)
                                                    ->orWhere('username', $o, $v);
                                            });
                                        } else {
                                            $q->where($f, $o, $v);
                                        }
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $field = 'name';
                    $operator = 'like';

                    if (! empty($search[0])) {
                        $field = $search[0];
                    }

                    if (! empty($search[1])) {
                        $operator = $search[1];
                    }

                    $value = $search[2];

                    if (($field === 'name' || $field === 'username' || $field === 'display_name' || $field === 'role.name' || $field === 'location.name') && $operator === 'like' && is_string($value)) {
                        $value = '%'.$value.'%';
                    }

                    if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                        if (isset($search[5])) {
                            $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                $table = $q->getModel()->getTable();

                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($table.'.'.$search[6]);
                                    } else {
                                        $q->whereNull($table.'.'.$search[6]);
                                    }
                                } else {
                                    $q->where($table.'.'.$search[6], $operator, $value);
                                }
                            });
                        } else {
                            if ($value === null) {
                                if ($operator === '!=' || $operator === '<>') {
                                    if ($field === 'display_name') {
                                        $q->whereNotNull('name');
                                    } else {
                                        $q->whereNotNull($field);
                                    }
                                } else {
                                    if ($field === 'display_name') {
                                        $q->whereNull('name');
                                    } else {
                                        $q->whereNull($field);
                                    }
                                }
                            } else {
                                if ($field === 'display_name') {
                                    $q->where(function ($q) use ($operator, $value) {
                                        $q->where('name', $operator, $value)
                                            ->orWhere('username', $operator, $value);
                                    });
                                } else {
                                    $q->where($field, $operator, $value);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        try {
            foreach ($sorts as $sort) {
                if ($q->getModel()->isFillable($sort[0]) || $sort[0] == 'id') {
                    $q->orderBy($sort[0], $sort[1]);
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        if ($trashed) {
            $q->withTrashed();
        }

        if ($includes) {
            $q->with($includes);
        }

        if ($excludes) {
            $q->without($excludes);
        }

        $pagination = null;

        // $q->with('dealer', 'assignees.user', 'photos', 'inputs.type');

        if ($limit || $request->page) {
            $users = $q->paginate($limit, $filters, 'page', $request->page);
            $users = $users->toArray();

            $pagination = $users['pagination'];
            $users = $users['data'];
        } else {
            $users = $q->get($filters)->toArray();
        }

        $response = [
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'users' => collect($users)->map(function ($item) {
                    if (isset($item['locations'])) {
                        if (! count($item['locations'])) {
                            unset($item['locations']);
                            $item['location'] = null;
                        } else {
                            $item['location'] = $item['locations'][0];
                        }
                    }

                    return $item;
                }),
            ],
            'meta' => [],
        ];

        if ($pagination) {
            $response['meta']['pagination'] = $pagination;
        }

        if (is_array($response['meta']) && ! count($response['meta'])) {
            $response['meta'] = new \stdClass();
        }

        return response()->json($response);
    }

    /**
     * Display currently authenticated user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function me(Request $request)
    {
        $user = $request->user();

        $user->load('role');
        $user->setAttribute('location', $user->locations->first());
        $user->unsetRelation('locations');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'user' => $user,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
