<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Models\Assignment;
use App\Models\AssignmentInput;

class AssignmentInputController extends Controller
{
    use Concerns\RequestQueryParamBuilder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $assignmentId)
    {
        $assignment = Assignment::find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $searches = $this->getRequestSearch($request);
        $filters = $this->getRequestFilter($request);
        $limit = $this->getRequestLimit($request);
        $trashed = $this->getRequestTrashed($request);
        $sorts = $this->getRequestSort($request);
        $includes = $this->getRequestIncludes($request);
        $excludes = $this->getRequestExcludes($request);

        $q = AssignmentInput::where('assignment_id', $assignment->id);
        $model = $q->getModel();

        try {
            foreach ($searches as $search) {
                if (isset($search[3])) {
                    $q->where(function ($q) use ($search, $model) {
                        $field = 'type.name';
                        $operator = '=';

                        if (! empty($search[0])) {
                            $field = $search[0];
                        }

                        if (! empty($search[1])) {
                            $operator = $search[1];
                        }

                        if ($field === 'type.name' && ! isset($search[5])) {
                            $search[5] = 'type';
                            $search[6] = 'name';
                        }

                        $value = $search[2];

                        if (($field === 'type.name') && $operator === 'like' && is_string($value)) {
                            $value = '%'.$value.'%';
                        }

                        if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                            if (isset($search[5])) {
                                $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                    $table = $q->getModel()->getTable();

                                    if ($value === null) {
                                        if ($operator === '!=' || $operator === '<>') {
                                            $q->whereNotNull($table.'.'.$search[6]);
                                        } else {
                                            $q->whereNull($table.'.'.$search[6]);
                                        }
                                    } else {
                                        $q->where($table.'.'.$search[6], $operator, $value);
                                    }
                                });
                            } else {
                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($field);
                                    } else {
                                        $q->whereNull($field);
                                    }
                                } else {
                                    $q->where($field, $operator, $value);
                                }
                            }
                        }

                        foreach ($search[3] as $s) {
                            $f = 'type.name';
                            $o = '=';

                            if (! empty($s[0])) {
                                $f = $s[0];
                            }

                            if (! empty($s[1])) {
                                $o = $s[1];
                            }

                            $v = $s[2];

                            if ($f === 'type.name' && ! isset($s[5])) {
                                $s[5] = 'type';
                                $s[6] = 'name';
                            }

                            if (($f === 'type.name') && $o === 'like' && is_string($v)) {
                                $v = '%'.$v.'%';
                            }

                            if ($model->isFillable($f) || $f == $model->getKeyName() || isset($s[5])) {
                                if (isset($s[5])) {
                                    $q->whereHas($s[5], function ($q) use ($s, $f, $o, $v) {
                                        $table = $q->getModel()->getTable();

                                        if ($v === null) {
                                            if ($o === '!=' || $o === '<>') {
                                                $q->whereNotNull($table.'.'.$s[6]);
                                            } else {
                                                $q->whereNull($table.'.'.$s[6]);
                                            }
                                        } else {
                                            $q->where($table.'.'.$s[6], $o, $v);
                                        }
                                    });
                                } else {
                                    if ($v === null) {
                                        if ($o === '!=' || $o === '<>') {
                                            $q->whereNotNull($f);
                                        } else {
                                            $q->whereNull($f);
                                        }
                                    } else {
                                        $q->where($f, $o, $v);
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $field = 'type.name';
                    $operator = 'like';

                    if (! empty($search[0])) {
                        $field = $search[0];
                    }

                    if (! empty($search[1])) {
                        $operator = $search[1];
                    }

                    $value = $search[2];

                    if ($field === 'type.name' && ! isset($search[5])) {
                        $search[5] = 'type';
                        $search[6] = 'name';
                    }

                    if (($field === 'type.name') && $operator === 'like' && is_string($value)) {
                        $value = '%'.$value.'%';
                    }

                    if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                        if (isset($search[5])) {
                            $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                $table = $q->getModel()->getTable();

                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($table.'.'.$search[6]);
                                    } else {
                                        $q->whereNull($table.'.'.$search[6]);
                                    }
                                } else {
                                    $q->where($table.'.'.$search[6], $operator, $value);
                                }
                            });
                        } else {
                            if ($value === null) {
                                if ($operator === '!=' || $operator === '<>') {
                                    $q->whereNotNull($field);
                                } else {
                                    $q->whereNull($field);
                                }
                            } else {
                                $q->where($field, $operator, $value);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        try {
            foreach ($sorts as $sort) {
                if ($q->getModel()->isFillable($sort[0]) || $sort[0] == 'id') {
                    $q->orderBy($sort[0], $sort[1]);
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        if ($trashed) {
            $q->withTrashed();
        }

        if ($includes) {
            $q->with($includes);
        }

        if ($excludes) {
            $q->without($excludes);
        }

        $pagination = null;

        if ($limit || $request->page) {
            $inputs = $q->paginate($limit, $filters, 'page', $request->page);
            $inputs = $inputs->toArray();

            $pagination = $inputs['pagination'];
            $inputs = $inputs['data'];
        } else {
            $inputs = $q->get($filters);
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner');

        $response = [
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'inputs' => $inputs,
                'assignment' => $assignment,
            ],
            'meta' => [],
        ];

        if ($pagination) {
            $response['meta']['pagination'] = $pagination;
        }

        if (is_array($response['meta']) && ! count($response['meta'])) {
            $response['meta'] = new \stdClass();
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $assignmentId)
    {
        $brandTypeIds = [];
        $qtys = [];

        $savedInputs = [];

        if (is_array($request->brand_type_id)) {
            $request->validate([
                'brand_type_id.*' => ['bail', 'required', Rule::exists('brand_types', 'id')],
                'qty.*' => ['required', 'integer'],
            ]);
            $brandTypeIds = $request->brand_type_id;
            $qtys = $request->qty;
        } else {
            $request->validate([
                'brand_type_id' => ['bail', 'required', Rule::exists('brand_types', 'id')],
                'qty' => ['required', 'integer'],
            ]);

            $brandTypeIds = [$request->brand_type_id];
            $qtys = [$request->qty];
        }

        $assignment = Assignment::find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $uniqueBrandTypeIds = [];

        foreach ($brandTypeIds as $index => $brandTypeId) {
            if (! array_key_exists($index, $qtys)) {
                return response()->json([
                    'status' => 'fail',
                    'code' => 'validation',
                    'message' => 'Qty for brand type: ' . $brandTypeId. ' is required',
                    'errors' => [
                        'qty' => 'Qty for brand type: ' . $brandTypeId. ' is required',
                    ],
                ]);
            }

            if (isset($uniqueBrandTypeIds[$brandTypeId])) {
                unset($brandTypeIds[$uniqueBrandTypeIds[$brandTypeId]]);
                unset($qtys[$uniqueBrandTypeIds[$brandTypeId]]);
            } else {
                $uniqueBrandTypeIds[$brandTypeId] = $index;
            }
        }

        foreach ($brandTypeIds as $index => $brandTypeId) {
            $input = $assignment->inputs()->where('brand_type_id', $brandTypeId)->first();

            if (! $input) {
                $input = $assignment->inputs()->create([
                    'brand_type_id' => $brandTypeId,
                    'assignment_id' => $assignment->id,
                    'qty' => $qtys[$index] ?? 0,
                    'created_by' => Auth::guard('api')->user()->id,
                ]);
            } else {
                $input->qty = $qtys[$index] ?? 0;
                $input->updated_by = Auth::guard('api')->user()->id;
                $input->save();
            }

            $savedInputs[] = $input;
        }

        if (count($savedInputs)) {
            $assignment->updated_by = Auth::guard('api')->user()->id;
            $assignment->save();
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'saved_inputs' => $savedInputs,
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $assignmentId, $id)
    {
        $assignment = Assignment::find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $request->validate([
            'brand_type_id' => ['required', Rule::exists('brand_types', 'id')],
            'qty' => ['required', 'integer'],
        ]);

        $input = AssignmentInput::where('assignment_id', $assignmentId)->find($id);

        if (! $input) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find input',
            ], 404);
        }

        $input->qty = $request->qty;
        $input->updated_by = Auth::guard('api')->user()->id;

        if (! $input->save()) {
            return response()->json([
                'status' => 'error',
                'code' => 'general',
                'message' => 'Failed to update input. Database error.',
            ], 500);
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'saved_input' => $input,
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($assignmentId, $id)
    {
        $assignment = Assignment::find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $input = AssignmentInput::where('assignment_id', $assignment->id)->find($id);

        if (! $input) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find input',
            ], 404);
        }


        if ($input->delete()) {
            $assignment->updated_by = Auth::guard('api')->user()->id;
            $assignment->save();

            $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner');

            return response()->json([
                'status' => 'success',
                'message' => 'Success!',
                'data' => [
                    'assignment' => $assignment,
                ],
            ]);
        }

        return response()->json([
            'status' => 'error',
            'code' => 'general',
            'message' => 'Failed to delete input. Database error.',
        ], 500);
    }
}
