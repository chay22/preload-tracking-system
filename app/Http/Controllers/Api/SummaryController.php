<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Assignment;
use App\Models\Brand;
use Exception;
use Carbon\Carbon;

class SummaryController extends Controller
{
    use Concerns\RequestQueryParamBuilder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function brand(Request $request)
    {
        $searches = $this->getRequestSearch($request);
        $filters = $this->getRequestFilter($request);
        $limit = $this->getRequestLimit($request);
        $trashed = $this->getRequestTrashed($request);
        $sorts = $this->getRequestSort($request);
        $includes = $this->getRequestIncludes($request);
        $excludes = $this->getRequestExcludes($request);

        $user = Auth::guard('api')->user();

        $q = Assignment::with([
            'assistants' => function ($q) use ($user) {
                $q->where('id', $user->id);
            },
            'owner' => function ($q) use ($user) {
                $q->where('id', $user->id);
            }
        ]);

        $model = $q->getModel();

        if (isset($searches['brand_id'])) {
            $q->with([
                'inputs.type.brand' => function ($q) use ($searches) {
                    $q->where('brands.id', $searches[2] ?? $searches[1]);
                }
            ]);
        } else {
            $q->with('inputs');
        }

        if (! isset($searches['created_at'])) {
            $searches['created_at'] = [
                'created_at',
                '=',
                date('Y-m-d'),
            ];
        }

        try {
            foreach ($searches as $search) {
                if (isset($search[3])) {
                    $q->where(function ($q) use ($search, $model) {
                        $field = 'brand_id';
                        $operator = '=';

                        if (! empty($search[0])) {
                            $field = $search[0];
                        }

                        if (! empty($search[1])) {
                            $operator = $search[1];
                        }

                        $value = $search[2];

                        if (($field === 'brand_name') && $operator === 'like' && is_string($value)) {
                            $value = '%'.$value.'%';
                        }

                        if ($field === 'brand_id' && $value) {
                            $q->whereHas('inputs.type.brand', function ($q) use ($value) {
                                $q->where('brands.id', $value);
                            });
                        } else if ($field === 'created_at' && $value) {
                            $q->whereMonth('created_at', Carbon::parse($value)->format('m'))->whereYear('created_at', Carbon::parse($value)->format('Y'));
                        } else if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                            if (isset($search[5])) {
                                $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                    $table = $q->getModel()->getTable();

                                    if ($value === null) {
                                        if ($operator === '!=' || $operator === '<>') {
                                            $q->whereNotNull($table.'.'.$search[6]);
                                        } else {
                                            $q->whereNull($table.'.'.$search[6]);
                                        }
                                    } else {
                                        $q->where($table.'.'.$search[6], $operator, $value);
                                    }
                                });
                            } else {
                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($field);
                                    } else {
                                        $q->whereNull($field);
                                    }
                                } else {
                                    $q->where($field, $operator, $value);
                                }
                            }
                        }

                        foreach ($search[3] as $s) {
                            $f = 'brand_id';
                            $o = '=';

                            if (! empty($s[0])) {
                                $f = $s[0];
                            }

                            if (! empty($s[1])) {
                                $o = $s[1];
                            }

                            $v = $s[2];

                            if (($f === 'brand_name') && $o === 'like' && is_string($v)) {
                                $v = '%'.$v.'%';
                            }

                            if ($f === 'brand_id' && $v) {
                                $q->whereHas('inputs.type.brand', function ($q) use ($v) {
                                    $q->where('brands.id', $v);
                                });
                            } else if ($f === 'created_at') {
                                $q->whereMonth('created_at', Carbon::parse($v)->format('m'))->whereYear('created_at', Carbon::parse($v)->format('Y'));
                            } else if ($model->isFillable($f) || $f == $model->getKeyName() || isset($s[5])) {
                                if (isset($s[5])) {
                                    $q->whereHas($s[5], function ($q) use ($s, $f, $o, $v) {
                                        $table = $q->getModel()->getTable();

                                        if ($v === null) {
                                            if ($o === '!=' || $o === '<>') {
                                                $q->whereNotNull($table.'.'.$s[6]);
                                            } else {
                                                $q->whereNull($table.'.'.$s[6]);
                                            }
                                        } else {
                                            $q->where($table.'.'.$s[6], $o, $v);
                                        }
                                    });
                                } else {
                                    if ($v === null) {
                                        if ($o === '!=' || $o === '<>') {
                                            $q->whereNotNull($f);
                                        } else {
                                            $q->whereNull($f);
                                        }
                                    } else {
                                        $q->where($f, $o, $v);
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $field = 'brand_id';
                    $operator = '=';

                    if (! empty($search[0])) {
                        $field = $search[0];
                    }

                    if (! empty($search[1])) {
                        $operator = $search[1];
                    }

                    $value = $search[2];

                    if (($field === 'brand_name') && $operator === 'like' && is_string($value)) {
                        $value = '%'.$value.'%';
                    }

                    if ($field === 'brand_id' && $value) {
                        $q->whereHas('inputs.type.brand', function ($q) use ($value) {
                            $q->where('brands.id', $value);
                        });
                    } else if ($field === 'created_at' && $value) {
                        $q->whereMonth('created_at', Carbon::parse($value)->format('m'))->whereYear('created_at', Carbon::parse($value)->format('Y'));
                    } else if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                        if (isset($search[5])) {
                            $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                $table = $q->getModel()->getTable();

                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($table.'.'.$search[6]);
                                    } else {
                                        $q->whereNull($table.'.'.$search[6]);
                                    }
                                } else {
                                    $q->where($table.'.'.$search[6], $operator, $value);
                                }
                            });
                        } else {
                            if ($value === null) {
                                if ($operator === '!=' || $operator === '<>') {
                                    $q->whereNotNull($field);
                                } else {
                                    $q->whereNull($field);
                                }
                            } else {
                                $q->where($field, $operator, $value);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        try {
            foreach ($sorts as $sort) {
                if ($q->getModel()->isFillable($sort[0]) || $sort[0] == 'id') {
                    $q->orderBy($sort[0], $sort[1]);
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        if ($trashed) {
            $q->withTrashed();
        }

        if ($includes) {
            $q->with($includes);
        }

        if ($excludes) {
            $q->without($excludes);
        }

        $pagination = null;

        if ($limit || $request->page) {
            $summary = $q->paginate($limit, $filters, 'page', $request->page);
            $summary = $summary->toArray();

            $pagination = $summary['pagination'];
            $summary = $summary['data'];
        } else {
            $summary = $q->get($filters);
        }


        $assignments = [];
        $users = [];
        $i = 1;

        foreach ($summary as $item) {
            $lastModifiedAt = ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i');
            $firstInput = $item->inputs->first();

            if (! $firstInput || ! $firstInput->brand) {
                continue;
            }

            $assignment = [
                'brand' => $firstInput ? $firstInput->brand->toArray() : null,
            ];

            foreach ($item->users as $worker) {
                if (! isset($assignments[$firstInput->brand->id.$worker->username])) {
                    $assignments[$firstInput->brand->id.$worker->username] = array_merge($assignment, [
                        'display_name' => $worker->display_name,
                        'username' => $worker->username,
                        'count' => $worker->count,
                        'type' => $worker->type,
                    ]);
                } else {
                    $assignments[$firstInput->brand->id.$worker->username]['count'] += $worker->count;
                }

                if (! isset($users[$worker->username])) {
                    $users[$worker->username] = [
                        'id' => $worker->id,
                        'display_name' => $worker->display_name,
                        'username' => $worker->username,
                        'total' => $worker->count,
                    ];
                } else {
                    $users[$worker->username]['total'] += $worker->count;
                }

                $i++;
            }
        }

        ksort($users);

        $response = [
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'summary' => array_values($assignments),
            ],
            'meta' => [],
        ];

        if ($pagination) {
            $response['meta']['pagination'] = $pagination;
        }

        if (is_array($response['meta']) && ! count($response['meta'])) {
            $response['meta'] = new \stdClass();
        }

        return response()->json($response);
    }

    protected function getAssignmentUser()
    {
        $user = $this->owner;

        if (! $user) {
            return [];
        }

        $user->attributes['type'] = 'owner';
        $users = [$user];

        foreach ($this->getRelation('assistants') as $assistant) {
            $assistant->attributes['type'] = 'assistant';
            $users[] = $assistant;
        }

        $userCount = count($users);

        $totalCount = $this->inputs->sum('success_count');
        $this->attributes['total_count'] = $totalCount;
        $count = round($totalCount / $userCount, 2, PHP_ROUND_HALF_DOWN);

        foreach ($users as $user) {
            $user->attributes['count'] = $count;
            $user->attributes['total_count'] = $totalCount;
        }

        return $users;
    }
}
