<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Models\Assignment;
use App\Models\AssignmentInput;
use App\Models\AssignmentPhoto;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;
use App\Models\User;

class AssignmentController extends Controller
{
    use Concerns\RequestQueryParamBuilder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searches = $this->getRequestSearch($request);
        $filters = $this->getRequestFilter($request);
        $limit = $this->getRequestLimit($request);
        $trashed = $this->getRequestTrashed($request);
        $sorts = $this->getRequestSort($request);
        $includes = $this->getRequestIncludes($request);
        $excludes = $this->getRequestExcludes($request);

        $q = Assignment::query();

        $model = $q->getModel();

        try {
            foreach ($searches as $search) {
                if (isset($search[3])) {
                    $q->where(function ($q) use ($search, $model) {
                        $field = 'name';
                        $operator = '=';

                        if (! empty($search[0])) {
                            $field = $search[0];
                        }

                        if (! empty($search[1])) {
                            $operator = $search[1];
                        }

                        $value = $search[2];

                        if (($field === 'name' || $field === 'dealer.name') && $operator === 'like' && is_string($value)) {
                            $value = '%'.$value.'%';
                        }

                        if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                            if (isset($search[5])) {
                                $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                    $table = $q->getModel()->getTable();

                                    if ($value === null) {
                                        if ($operator === '!=' || $operator === '<>') {
                                            $q->whereNotNull($table.'.'.$search[6]);
                                        } else {
                                            $q->whereNull($table.'.'.$search[6]);
                                        }
                                    } else {
                                        $q->where($table.'.'.$search[6], $operator, $value);
                                    }
                                });
                            } else {
                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($field);
                                    } else {
                                        $q->whereNull($field);
                                    }
                                } else {
                                    $q->where($field, $operator, $value);
                                }
                            }
                        }

                        foreach ($search[3] as $s) {
                            $f = 'name';
                            $o = '=';

                            if (! empty($s[0])) {
                                $f = $s[0];
                            }

                            if (! empty($s[1])) {
                                $o = $s[1];
                            }

                            $v = $s[2];

                            if (($f === 'name' || $f === 'dealer.name') && $o === 'like' && is_string($v)) {
                                $v = '%'.$v.'%';
                            }

                            if ($f->isFillable($f) || $f == $model->getKeyName() || isset($s[5])) {
                                if (isset($s[5])) {
                                    $q->whereHas($s[5], function ($q) use ($s, $f, $o, $v) {
                                        $table = $q->getModel()->getTable();

                                        if ($v === null) {
                                            if ($o === '!=' || $o === '<>') {
                                                $q->whereNotNull($table.'.'.$s[6]);
                                            } else {
                                                $q->whereNull($table.'.'.$s[6]);
                                            }
                                        } else {
                                            $q->where($table.'.'.$s[6], $o, $v);
                                        }
                                    });
                                } else {
                                    if ($v === null) {
                                        if ($o === '!=' || $o === '<>') {
                                            $q->whereNotNull($f);
                                        } else {
                                            $q->whereNull($f);
                                        }
                                    } else {
                                        $q->where($f, $o, $v);
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $field = 'name';
                    $operator = 'like';

                    if (! empty($search[0])) {
                        $field = $search[0];
                    }

                    if (! empty($search[1])) {
                        $operator = $search[1];
                    }

                    $value = $search[2];

                    if (($field === 'name' || $field === 'dealer.name') && $operator === 'like' && is_string($value)) {
                        $value = '%'.$value.'%';
                    }

                    if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                        if (isset($search[5])) {
                            $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                $table = $q->getModel()->getTable();

                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($table.'.'.$search[6]);
                                    } else {
                                        $q->whereNull($table.'.'.$search[6]);
                                    }
                                } else {
                                    $q->where($table.'.'.$search[6], $operator, $value);
                                }
                            });
                        } else {
                            if ($value === null) {
                                if ($operator === '!=' || $operator === '<>') {
                                    $q->whereNotNull($field);
                                } else {
                                    $q->whereNull($field);
                                }
                            } else {
                                $q->where($field, $operator, $value);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        try {
            foreach ($sorts as $sort) {
                if ($q->getModel()->isFillable($sort[0]) || $sort[0] == 'id') {
                    $q->orderBy($sort[0], $sort[1]);
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        if ($trashed) {
            $q->withTrashed();
        }

        if ($includes) {
            $q->with($includes);
        }

        $q->with('dealer', 'photos', 'inputs.type.brand', 'inputs.createdBy', 'inputs.updatedBy', 'assistants');

        if ($excludes) {
            $q->without($excludes);
        }

        $pagination = null;


        if ($limit || $request->page) {
            $assignments = $q->paginate($limit, $filters, 'page', $request->page);
            $assignments = $assignments->toArray();

            $pagination = $assignments['pagination'];
            $assignments = $assignments['data'];
        } else {
            $assignments = $q->get($filters)->toArray();
        }

        $assignments = array_map(function ($item) {
            if (isset($item['assistants']) && count($item['assistants'])) {
                $assistants = [];

                foreach ($item['assistants'] as $user) {
                    if (isset($user['pivot'])) {
                        unset($user['pivot']);
                    }

                    $assistants[] = $user;
                }

                $item['assistants'] = $assistants;
            } else {
                $item['assistants'] = null;
            }

            return $item;
        }, $assignments);

        $response = [
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignments' => $assignments,
            ],
            'meta' => [],
        ];

        if ($pagination) {
            $response['meta']['pagination'] = $pagination;
        }

        if (is_array($response['meta']) && ! count($response['meta'])) {
            $response['meta'] = new \stdClass();
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'dealer_id' => ['required', 'integer', Rule::exists('dealers', 'id')],
            'tool_id' => ['required', Rule::exists('tools', 'id')],
            'assigned_at' => ['required', 'date'],
        ]);

        $brandTypeIds = [];
        $successCounts = [];
        $failedCounts = [];

        $savedInputs = [];

        if ($request->inputs) {
            $inputs = $request->inputs;

            if (is_array($inputs)) {
                $request->validate([
                    'inputs.*.brand_type_id' => ['bail', 'required', 'integer', Rule::exists('brand_types', 'id')],
                    'inputs.*.success_count' => ['sometimes', 'nullable', 'integer'],
                    'inputs.*.failed_count' => ['sometimes', 'nullable', 'integer'],
                ]);

                foreach ($inputs as $key => $value) {
                    $brandTypeIds[$key] = $value['brand_type_id'];
                    $successCounts[$key] = $value['success_count'] ?? 0;
                    $failedCounts[$key] = $value['failed_count'] ?? 0;
                }
            }
        }

        $photos = [];

        if (is_array($request->photos)) {
            $request->validate([
                'photos.*' => ['sometimes', 'bail', 'required', 'max:'.((2*(1024*1024))*2)]
            ]);

            $photos = $request->photos;
        } else if ($request->photos) {
            $request->validate([
                'photos' => ['sometimes', 'bail', 'required', 'max:'.((2*(1024*1024))*2)]
            ]);
            $photos = [$request->photos];
        }

        $assistantIds = [];

        if ($request->assistants) {
            $assistantIds = $request->assistants;

            if (! is_array($assistantIds)) {
                $assistantIds = [$assistantIds];
            }

            foreach ($assistantIds as $assistantId) {
                if (! User::where('id', $assistantIds)->whereHas('role', function ($q) {
                    $q->where('key', 'assistant');
                })->first()) {
                    return response()->json([
                        'status' => 'fail',
                        'code' => 'not_found',
                        'message' => 'Failed to find assistant with ID: ' . $assistantId,
                    ], 403);
                }
            }
        }

        $user = Auth::guard('api')->user();
        $data = $request->all();
        $data['assigned_at'] = Carbon::parse($request->assigned_at);
        $data['created_by'] = $user->id;
        $data['status'] = $request->status ? $request->status : 0;

        $assignment = Assignment::create($data);
        $savedInputs = [];

        if (count($brandTypeIds)) {
            $uniqueBrandTypeIds = [];

            foreach ($brandTypeIds as $index => $brandTypeId) {
                if (isset($uniqueBrandTypeIds[$brandTypeId])) {
                    unset($brandTypeIds[$uniqueBrandTypeIds[$brandTypeId]]);
                    unset($successCounts[$uniqueBrandTypeIds[$brandTypeId]]);
                    unset($failedCounts[$uniqueBrandTypeIds[$brandTypeId]]);
                } else {
                    $uniqueBrandTypeIds[$brandTypeId] = $index;
                }
            }

            foreach ($brandTypeIds as $index => $brandTypeId) {
                $input = $assignment->inputs()->where('brand_type_id', $brandTypeId)->first();

                if (! $input) {
                    $input = $assignment->inputs()->create([
                        'brand_type_id' => $brandTypeId,
                        'assignment_id' => $assignment->id,
                        'success_count' => $successCounts[$index] ?? 0,
                        'failed_count' => $failedCounts[$index] ?? 0,
                        'created_by' => $user->id,
                    ]);
                } else {
                    $input->success_count = $successCounts[$index] ?? 0;
                    $input->failed_count = $failedCounts[$index] ?? 0;
                    $input->updated_by = $user->id;
                    $input->save();
                }

                $savedInputs[] = $input;
            }
        }

        $savedPhotos = [];
        if (count($photos)) {
            AssignmentPhoto::where('assignment_id', $assignment->id)->delete();

            foreach ($photos as $photo) {
                $savedPhotos[] = AssignmentPhoto::upload($assignment, $photo);
            }
        }

        if (count($assistantIds)) {
            $assignment->assistants()->sync($assistantIds);
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner', 'assistants');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assignment = Assignment::find($id);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $request->validate([
            'dealer_id' => ['sometimes', 'integer', Rule::exists('dealers', 'id')],
            'tool_id' => ['sometimes', Rule::exists('tools', 'id')],
            'assigned_at' => ['sometimes', 'date'],
        ]);

        if ($request->tool_id) {
            $assignment->tool_id = $request->tool_id;
        }

        if ($request->dealer_id) {
            $assignment->dealer_id = $request->dealer_id;
        }

        if ($request->assigned_at) {
            $assignment->assigned_at = $request->assigned_at;
        }

        $assignment->status = $request->status ? $request->status : 0;

        $inputIds = [];
        $brandTypeIds = [];
        $successCounts = [];
        $failedCounts = [];

        $isDirty = false;

        if ($request->inputs) {
            $inputs = $request->inputs;

            if (is_array($inputs)) {
                $request->validate([
                    'inputs.*.brand_type_id' => ['bail', 'required', 'integer', Rule::exists('brand_types', 'id')],
                    'inputs.*.success_count' => ['sometimes', 'nullable', 'integer'],
                    'inputs.*.failed_count' => ['sometimes', 'nullable', 'integer'],
                ]);

                foreach ($inputs as $key => $value) {
                    $inputIds[$key] = $value['id'] ?? null;
                    $brandTypeIds[$key] = $value['brand_type_id'];
                    $successCounts[$key] = $value['success_count'] ?? 0;
                    $failedCounts[$key] = $value['failed_count'] ?? 0;
                }
            }
        }

        $photos = [];

        if (is_array($request->photos)) {
            $request->validate([
                'photos.*' => ['sometimes', 'bail', 'required', 'max:'.((2*(1024*1024))*2)]
            ]);

            $photos = $request->photos;
        } else if ($request->photos) {
            $request->validate([
                'photos' => ['sometimes', 'bail', 'required', 'max:'.((2*(1024*1024))*2)]
            ]);
            $photos = [$request->photos];
        }

        if ($request->assistants) {
            $assistantIds = $request->assistants;

            if (! is_array($assistantIds)) {
                $assistantIds = [$assistantIds];
            }

            foreach ($assistantIds as $assistantId) {
                if (! User::where('id', $assistantId)->whereHas('role', function ($q) {
                    $q->where('key', 'assistant');
                })->first()) {
                    return response()->json([
                        'status' => 'fail',
                        'code' => 'not_found',
                        'message' => 'Failed to find assistant with ID: ' . $assistantId,
                    ], 404);
                }
            }

            $assignment->assistants()->sync($assistantIds);
        }

        if (count($brandTypeIds)) {
            $assignment->inputs()->delete();

            $uniqueBrandTypeIds = [];

            foreach ($brandTypeIds as $index => $brandTypeId) {
                if (isset($uniqueBrandTypeIds[$brandTypeId])) {
                    unset($brandTypeIds[$uniqueBrandTypeIds[$brandTypeId]]);
                    unset($successCounts[$uniqueBrandTypeIds[$brandTypeId]]);
                    unset($failedCounts[$uniqueBrandTypeIds[$brandTypeId]]);
                } else {
                    $uniqueBrandTypeIds[$brandTypeId] = $index;
                }
            }

            foreach ($brandTypeIds as $index => $brandTypeId) {
                $input = $assignment->inputs()->where('brand_type_id', $brandTypeId)->first();

                if (! $input) {
                    $input = $assignment->inputs()->create([
                        'brand_type_id' => $brandTypeId,
                        'assignment_id' => $assignment->id,
                        'success_count' => $successCounts[$index] ?? 0,
                        'failed_count' => $failedCounts[$index] ?? 0,
                        'created_by' => Auth::guard('api')->user()->id,
                    ]);
                } else {
                    $input->success_count = $successCounts[$index] ?? 0;
                    $input->failed_count = $failedCounts[$index] ?? 0;
                    $input->updated_by = Auth::guard('api')->user()->id;
                    $input->save();
                }

                $isDirty = true;
            }
        }

        if (count($photos)) {
            AssignmentPhoto::where('assignment_id', $assignment->id)->delete();

            foreach ($photos as $photo) {
                AssignmentPhoto::upload($assignment, $photo);
                $isDirty = true;
            }
        }

        if ($assignment->isDirty() || $isDirty) {
            $assignment->updated_by = Auth::guard('api')->user()->id;
            $assignment->save();
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner', 'assistants');

        $assistants = $assignment->getRelation('assistants');

        if ($assistants) {
            $assistants = $assistants->map(function ($item) {
                $item->unsetRelation('pivot');

                return $item;
            });
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Publish the assignment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function done(Request $request, $id)
    {
        $assignment = Assignment::find($id);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $assignment->status = 1;
        $assignment->updated_by = Auth::guard('api')->user()->id;
        $assignment->save();

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'tool', 'owner', 'assistants');

        $assistants = $assignment->getRelation('assistants');

        if ($assistants) {
            $assistants = $assistants->map(function ($item) {
                $item->unsetRelation('pivot');

                return $item;
            });
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = Assignment::find($id);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $assignment->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignment' => null,
            ],
        ]);
    }
}
