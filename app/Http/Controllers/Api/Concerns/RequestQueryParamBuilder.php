<?php

namespace App\Http\Controllers\Api\Concerns;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Str;

trait RequestQueryParamBuilder
{
    protected function getRequestSearch(Request $request)
    {
        if (! $request->search) {
            return [];
        }

        try {
            $searches = $request->search;

            if ($searches) {
                $searches = explode(';', $searches);
                $allSearches = [];

                foreach ($searches as $value) {
                    $value = explode(':', $value);
                    $field = null;
                    $operator = null;
                    $terms = null;

                    if (count($value) === 3) {
                        $field = $value[0];
                        $operator = $value[1];
                        $terms = $value[2];
                    } else if (count($value) === 2) {
                        $field = $value[0];
                        $operator = '=';
                        $terms = $value[1];
                    } else if (count($value) === 1) {
                        $field = null;
                        $operator = null;
                        $terms = $value[0];
                    }

                    if ($terms === 'null') {
                        $terms = null;
                    } else if ($terms === 'false') {
                        $terms = false;
                    } else if ($terms === 'true') {
                        $terms = true;
                    }

                    $allSearches[] = [$field, $operator, $terms];
                }

                $searches = [];

                foreach ($allSearches as $value) {
                    if (isset($value[0])) {
                        if (isset($searches[$value[0]])) {
                            if (! isset($searches[$value[0]][3])) {
                                $searches[$value[0]][3] = [$value];
                            } else {
                                $searches[$value[0]][3][] = $value;
                            }
                        } else {
                            $searches[$value[0]] = $value;
                        }
                    } else {
                        $searches[] = $value;
                    }
                }
            }

            $result = [];

            foreach ($searches as $key => $value) {
                if (strpos($value[0], '.') === false) {
                    $result[$key] = $value;
                } else {
                    $relations = explode('.', $key);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;

                    foreach ($relations as $i => $related) {
                        if ($i < $lastIndex) {
                            $relateds[] = Str::camel($related);
                        } else {
                            $lastRelated = $related;
                        }
                    }

                    $value[5] = implode('.', $relateds);
                    $value[0] = $value[5].'.'.$lastRelated;
                    $value[6] = $lastRelated;
                    $result[$value[0]] = $value;
                }
            }

            return $result;
        } catch (Exception $e) {
            throw $e;
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '', 0, $e);
        }
    }

    protected function getRequestFilter(Request $request)
    {
        if (! $request->filter) {
            return ['*'];
        }

        try {
            $filter = $request->filter;
            $filter = explode(';', $filter);

            return $filter;
        } catch (Exception $e) {
            throw new Exception('Invalid query "filter" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }
    }

    protected function getRequestSort(Request $request)
    {
        $sorts = $request->sort;

        if (! $sorts) {
            return [];
        }

        try {
            $sorts = explode(';', $sorts);
            $result = [];

            foreach ($sorts as $value) {
                $sort = explode(':', $value);
                $sortBy = null;
                $sortDir = null;

                if (count($sort) === 2) {
                    $sortBy = $sort[0];
                    $sortDir = $sort[1];
                } else if (count($sort) === 1) {
                    $sortBy = $sort[0];
                    $sortDir = 'asc';
                }

                $result[] = [$sortBy, $sortDir];
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }
    }

    protected function getRequestLimit(Request $request)
    {
        $limit = (int) $request->limit;

        return $limit;
    }

    protected function getRequestTrashed(Request $request)
    {
        $withTrashed = $request->trashed;

        if ($withTrashed === true || $withTrashed === 'true' || $withTrashed === '1' || $withTrashed === 1) {
            return true;
        }

        return false;
    }

    protected function getRequestIncludes(Request $request)
    {
        $includes = $request->include;

        if (! $includes) {
            return;
        }

        try {
            $includes = explode(',', $includes);

            return $includes;
        } catch (Exception $e) {
            throw new Exception('Invalid query "include" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }
    }

    protected function getRequestExcludes(Request $request)
    {
        $excludes = $request->exclude;

        if (! $excludes) {
            return;
        }

        try {
            $excludes = explode(',', $excludes);

            return $excludes;
        } catch (Exception $e) {
            throw new Exception('Invalid query "exclude" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }
    }
}
