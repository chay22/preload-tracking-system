<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Models\Assignment;
use App\Models\Assignee;
use App\Models\AssignmentInput;

class AssignmentAssigneeController extends Controller
{
    use Concerns\RequestQueryParamBuilder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $assignmentId)
    {
        $assignment = Assignment::forAssignee(Auth::user())->find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $searches = $this->getRequestSearch($request);
        $filters = $this->getRequestFilter($request);
        $limit = $this->getRequestLimit($request);
        $trashed = $this->getRequestTrashed($request);
        $sorts = $this->getRequestSort($request);
        $includes = $this->getRequestIncludes($request);
        $excludes = $this->getRequestExcludes($request);

        $q = Assignee::where('assignment_id', $assignment->id);
        $model = $q->getModel();

        try {
            foreach ($searches as $search) {
                if (isset($search[3])) {
                    $q->where(function ($q) use ($search, $model) {
                        $field = 'user.name';
                        $operator = '=';

                        if (! empty($search[0])) {
                            $field = $search[0];
                        }

                        if (! empty($search[1])) {
                            $operator = $search[1];
                        }

                        if ($field === 'user.name' && ! isset($search[5])) {
                            $search[5] = 'user';
                            $search[6] = 'name';
                        }

                        $value = $search[2];

                        if (($field === 'user.name') && $operator === 'like' && is_string($value)) {
                            $value = '%'.$value.'%';
                        }

                        if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                            if (isset($search[5])) {
                                $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                    $table = $q->getModel()->getTable();

                                    if ($value === null) {
                                        if ($operator === '!=' || $operator === '<>') {
                                            $q->whereNotNull($table.'.'.$search[6]);
                                        } else {
                                            $q->whereNull($table.'.'.$search[6]);
                                        }
                                    } else {
                                        $q->where($table.'.'.$search[6], $operator, $value);
                                    }
                                });
                            } else {
                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($field);
                                    } else {
                                        $q->whereNull($field);
                                    }
                                } else {
                                    $q->where($field, $operator, $value);
                                }
                            }
                        }

                        foreach ($search[3] as $s) {
                            $f = 'user.name';
                            $o = '=';

                            if (! empty($s[0])) {
                                $f = $s[0];
                            }

                            if (! empty($s[1])) {
                                $o = $s[1];
                            }

                            $v = $s[2];

                            if ($f === 'user.name' && ! isset($s[5])) {
                                $s[5] = 'user';
                                $s[6] = 'name';
                            }

                            if (($f === 'user.name') && $o === 'like' && is_string($v)) {
                                $v = '%'.$v.'%';
                            }

                            if ($model->isFillable($f) || $f == $model->getKeyName() || isset($s[5])) {
                                if (isset($s[5])) {
                                    $q->whereHas($s[5], function ($q) use ($s, $f, $o, $v) {
                                        $table = $q->getModel()->getTable();

                                        if ($v === null) {
                                            if ($o === '!=' || $o === '<>') {
                                                $q->whereNotNull($table.'.'.$s[6]);
                                            } else {
                                                $q->whereNull($table.'.'.$s[6]);
                                            }
                                        } else {
                                            $q->where($table.'.'.$s[6], $o, $v);
                                        }
                                    });
                                } else {
                                    if ($v === null) {
                                        if ($o === '!=' || $o === '<>') {
                                            $q->whereNotNull($f);
                                        } else {
                                            $q->whereNull($f);
                                        }
                                    } else {
                                        $q->where($f, $o, $v);
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $field = 'user.name';
                    $operator = 'like';

                    if (! empty($search[0])) {
                        $field = $search[0];
                    }

                    if (! empty($search[1])) {
                        $operator = $search[1];
                    }

                    $value = $search[2];

                    if ($field === 'user.name' && ! isset($search[5])) {
                        $search[5] = 'user';
                        $search[6] = 'name';
                    }

                    if (($field === 'user.name') && $operator === 'like' && is_string($value)) {
                        $value = '%'.$value.'%';
                    }

                    if ($model->isFillable($field) || $field == $model->getKeyName() || isset($search[5])) {
                        if (isset($search[5])) {
                            $q->whereHas($search[5], function ($q) use ($search, $field, $operator, $value) {
                                $table = $q->getModel()->getTable();

                                if ($value === null) {
                                    if ($operator === '!=' || $operator === '<>') {
                                        $q->whereNotNull($table.'.'.$search[6]);
                                    } else {
                                        $q->whereNull($table.'.'.$search[6]);
                                    }
                                } else {
                                    $q->where($table.'.'.$search[6], $operator, $value);
                                }
                            });
                        } else {
                            if ($value === null) {
                                if ($operator === '!=' || $operator === '<>') {
                                    $q->whereNotNull($field);
                                } else {
                                    $q->whereNull($field);
                                }
                            } else {
                                $q->where($field, $operator, $value);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "search" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        try {
            foreach ($sorts as $sort) {
                if ($q->getModel()->isFillable($sort[0]) || $sort[0] == 'id') {
                    $q->orderBy($sort[0], $sort[1]);
                }
            }
        } catch (Exception $e) {
            throw new Exception('Invalid query "sort" specified.' . config('app.debug') ? ' '.$e->getMessage() : '');
        }

        if ($trashed) {
            $q->withTrashed();
        }

        if ($includes) {
            $q->with($includes);
        }

        $q->with('user');

        if ($excludes) {
            $q->without($excludes);
        }

        $pagination = null;

        if ($limit || $request->page) {
            $assignees = $q->paginate($limit, $filters, 'page', $request->page);
            $assignees = $assignees->toArray();

            $pagination = $assignees['pagination'];
            $assignees = $assignees['data'];
        } else {
            $assignees = $q->get($filters);
        }

        $q->with('dealer', 'assignees.user', 'photos', 'inputs.type', 'owner');

        $response = [
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignees' => $assignees,
                'assignment' => $assignment,
            ],
            'meta' => [],
        ];

        if ($pagination) {
            $response['meta']['pagination'] = $pagination;
        }

        if (is_array($response['meta']) && ! count($response['meta'])) {
            $response['meta'] = new \stdClass();
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $assignmentId)
    {
        $userIds = [];

        $savedAssignees = [];

        if (is_array($request->user_id)) {
            $request->validate([
                'user_id.*' => ['bail', 'required', Rule::exists('users', 'id')],
            ]);
            $userIds = $request->user_id;
        } else {
            $request->validate([
                'user_id' => ['bail', 'required', Rule::exists('users', 'id')],
            ]);

            $userIds = [$request->user_id];
        }

        $assignment = Assignment::forAssignee(Auth::user())->find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $uniqueUserIds = array_unique($userIds);

        foreach ($uniqueUserIds as $index => $userId) {
            $assignee = Assignee::where('user_id', $userId)->first();

            if (! $assignee) {
                $assignee = $assignment->assignees()->create([
                    'user_id' => $userId,
                    'assignment_id' => $assignment->id,
                ]);
            }

            $savedAssignees[] = $assignee;
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'assignees.user', 'owner');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'saved_assignees' => $savedAssignees,
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $assignmentId, $id)
    {
        $assignment = Assignment::forAssignee(Auth::user())->find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $request->validate([
            'user_id' => ['required', Rule::exists('users', 'id')],
        ]);

        $assignee = Assignee::where('assignment_id', $assignmentId)->find($id);

        if (! $assignee) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignee',
            ], 404);
        }

        $assignee->user_id = $request->user_id;

        if (! $assignee->save()) {
            return response()->json([
                'status' => 'error',
                'code' => 'general',
                'message' => 'Failed to update assignee. Database error.',
            ], 500);
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'assignees.user', 'owner');

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'saved_assignee' => $assignee,
                'assignment' => $assignment,
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($assignmentId, $id)
    {
        $assignment = Assignment::forAssignee(Auth::user())->find($assignmentId);

        if (! $assignment) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignment',
            ], 404);
        }

        $assignee = Assignee::where('assignment_id', $assignment->id)->find($id);

        if (! $assignee) {
            return response()->json([
                'status' => 'fail',
                'code' => 'not_found',
                'message' => 'Failed to find assignee',
            ], 404);
        }

        if ($assignee->is_owner) {
            if ($assignee->user_id !== Auth::user()->id) {
                return response()->json([
                    'status' => 'fail',
                    'code' => 'general',
                    'message' => 'Only the owner can remove this user',
                ]);
            }

            $firstOtherAssignee = Assignment::find($assignment->id)->assignees()->where('id', '!=', $assignee->id)->first();

            if (! $firstOtherAssignee) {
                return response()->json([
                    'status' => 'fail',
                    'code' => 'general',
                    'message' => 'No assignee left for this assignment. Please delete the assignment instead',
                ]);
            }

            $firstOtherAssignee->is_owner = 1;
            $firstOtherAssignee->save();
        }

        $assignment->load('dealer', 'photos', 'inputs.type.brand', 'assignees.user', 'owner');

        if ($assignee->delete()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Success!',
                'data' => [
                    'assignment' => $assignment,
                ],
            ]);
        }

        return response()->json([
            'status' => 'error',
            'code' => 'general',
            'message' => 'Failed to delete assignee. Database error.',
        ], 500);
    }
}
