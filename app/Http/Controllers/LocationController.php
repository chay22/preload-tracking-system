<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\Location;
use App\Models\Dealer;
use App\Models\User;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('access.location');

        $pageFilters = null;
        $pageSort = null;
        $pageLimit = 25;
        $pageOffset = 0;
        $query = Location::query();

        if ($request->has('filter') && $request->filled('filter')) {
            $pageFilters = $request->filter;

            if (isset($pageFilters['global'])) {
                $query->where('id', 'like', '%'.$pageFilters['global'].'%');
            }

            if (isset(($pageFilters['status']))) {
                $pageFilters['status'] = (int) $pageFilters['status'];
                $query->where('status', $pageFilters['status']);
            }
        }

        if ($request->filled('sort')) {
            $pageSort = $request->sort;

            if (isset($pageSort['by'], $pageSort['dir'])) {
                $pageSort['dir'] = (int) $pageSort['dir'];

                if (strpos($pageSort['by'], '.') !== false) {
                    $relations = explode('.', $pageSort['by']);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;
                    $column = $relations[$lastIndex];
                    unset($relations[$lastIndex]);
                    $relations = implode('.', $relations);

                    $query->whereHas($relations, function ($q) use ($column, $pageSort) {
                        $q->orderBy($column, $pageSort['dir'] < 0 ? 'desc' : 'asc');
                    });
                } else {
                    $query->orderBy($pageSort['by'], $pageSort['dir'] < 0 ? 'desc' : 'asc');
                }
            }
        } else {
            $pageSort = [
                'by' => 'id',
                'dir' => -1,
            ];
            $query->orderBy('id', 'DESC');
        }

        if ($request->filled('limit')) {
            $pageLimit = $request->limit;
        }

        if ($request->filled('offset')) {
            $pageOffset = $request->offset;
        }

        $locations = $query->get();
        $totalRecords = count($locations);

        return Inertia::render('Location/Index', [
            'can' => [
                'create.location' => Auth::user()->can('create.location'),
                'edit.location' => Auth::user()->can('edit.location'),
                'delete.location' => Auth::user()->can('delete.location'),
            ],
            'locations' => $locations->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'status' => $item->status,
                    'active' => $item->active,
                    'last_modified_at' => ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i'),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                ];
            }),
            'page_filters' => $pageFilters,
            'page_sort' =>  $pageSort,
            'page_limit' => (int) $pageLimit,
            'page_offset' => (int) $pageOffset,
            'total_records' => (int) $totalRecords,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('edit.location');

        $request->validate([
            'name' => ['required'],
        ]);

        $data = $request->all();

        $data['status'] = $request->status ? $request->status : 0;

        $location = Location::create($data);

        Session::flash('success', 'Location succesfully created');

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $location = Location::findOrFail($id);

        // return Inertia::render('Location/Edit', [
        //     'location' => $location
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit.location');

        $location = Location::findOrFail($id);

        $location->name = $request->name;
        $location->status = (int) $request->status ? $request->status : 0;

        if ($location->isDirty()) {
            $location->save();
        }

        Session::flash('success', __('Location successfully updated'));

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::findOrFail($id);

        $location->delete();

        Session::flash('success', __('Location successfully deleted'));

        return Redirect::back();
    }
}
