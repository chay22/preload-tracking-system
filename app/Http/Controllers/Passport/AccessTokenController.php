<?php

namespace App\Http\Controllers\Passport;

use Laminas\Diactoros\Response as Psr7Response;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser as JwtParser;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as LaravelRequest;
use Laravel\Passport\Exceptions\OAuthServerException;
use Illuminate\Validation\ValidationException;
use Exception;

class AccessTokenController
{
    use HandlesOAuthErrors;

    /**
     * The authorization server.
     *
     * @var \League\OAuth2\Server\AuthorizationServer
     */
    protected $server;

    /**
     * The token repository instance.
     *
     * @var \Laravel\Passport\TokenRepository
     */
    protected $tokens;

    /**
     * The JWT parser instance.
     *
     * @var \Lcobucci\JWT\Parser
     */
    protected $jwt;

    /**
     * Create a new controller instance.
     *
     * @param  \League\OAuth2\Server\AuthorizationServer  $server
     * @param  \Laravel\Passport\TokenRepository  $tokens
     * @param  \Lcobucci\JWT\Parser  $jwt
     * @return void
     */
    public function __construct(AuthorizationServer $server,
                                TokenRepository $tokens,
                                JwtParser $jwt)
    {
        $this->jwt = $jwt;
        $this->server = $server;
        $this->tokens = $tokens;
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param  \Psr\Http\Message\ServerRequestInterface  $request
     * @return \Illuminate\Http\Response
     */
    public function issueToken(ServerRequestInterface $request)
    {
        try {
            return $this->withErrorHandling(function () use ($request) {
                $response = $this->server->respondToAccessTokenRequest($request, new Psr7Response);

                $body = json_decode($response->getBody(), true);
                $request = $this->rebuildRequest($request, $body);

                $data = [
                    'status' => 'success',
                    'message' => __('Succesfully logged in'),
                    'data' => [
                        'token' => $body,
                        'user' => Auth::guard('api')->setRequest($request)->user(),
                    ]
                ];

                return response()->json($data, $response->getStatusCode(), $response->getHeaders());
            });
        } catch (OAuthServerException $e) {
            return response()->json([
                'status' => 'fail',
                'code' => 'validation',
                'message' => trans('auth.failed'),
                'errors' => [
                    'username' => [trans('auth.failed')],
                ],
            ], 422);
        } catch(ValidationException $e) {
            throw $e;
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'code' => 'general',
                'message' => __('Failed to login.'),
            ], 400);
        }
    }

    protected function rebuildRequest(ServerRequestInterface $request, $body)
    {
        $headers = [];

        foreach ($request->getHeaders() as $key => $value) {
            if ($value && count($value) && isset($value[0])) {
                $headers[$key] = $value[0];
            }
        }

        $headers['Authorization'] = 'Bearer '.$body['access_token'];

        $request = LaravelRequest::capture()->duplicate(
            $request->getQueryParams(),
            null,
            null,
            null,
            null,
            $headers
        );

        $request->headers->replace($headers);

        return $request;
    }
}
