<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    Redirect,
    Session
};
use Illuminate\Validation\Rule;
use App\Models\Assignment;
use App\Models\AssignmentInput;
use App\Models\Dealer;
use Carbon\Carbon;
use Exception;

class AssignmentInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $assignmentId)
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request, $assignmentId)
    {
        $this->authorize('access.assignment');

        $assignment = Assignment::findOrFail($assignmentId);

        $pageFilters = null;
        $pageSort = null;
        $pageLimit = 25;
        $pageOffset = 0;
        $query = AssignmentInput::with('type.brand')->where('assignment_id', $assignment->id);

        if ($request->has('filter') && $request->filled('filter')) {
            $pageFilters = $request->filter;


        }

        if ($request->filled('sort')) {
            $pageSort = $request->sort;

            if (isset($pageSort['by'], $pageSort['dir'])) {
                $pageSort['id'] = (int) $pageSort['dir'];

                if (strpos($pageSort['by'], '.') !== false) {
                    $relations = explode('.', $pageSort['by']);
                    $relateds = $fullRelated = [];
                    $lastRelated = null;
                    $lastIndex = count($relations) - 1;
                    $column = $relations[$lastIndex];
                    unset($relations[$lastIndex]);
                    $relations = implode('.', $relations);

                    $query->whereHas($relations, function ($q) use ($column, $pageSort) {
                        $q->orderBy($column, $pageSort['dir'] < 0 ? 'desc' : 'asc');
                    });
                } else {
                    $query->orderBy($pageSort['by'], $pageSort['dir'] < 0 ? 'desc' : 'asc');
                }
            }
        } else {
            $pageSort = [
                'by' => 'id',
                'dir' => 1,
            ];
            $query->orderBy('id', 'DESC');
        }

        if ($request->filled('limit')) {
            $pageLimit = $request->limit;
        }

        if ($request->filled('offset')) {
            $pageOffset = $request->offset;
        }

        $inputs = $query->get();
        $totalRecords = count($inputs);

        return response()->json([
            'status' => 'success',
            'message' => 'Success!',
            'data' => [
                'assignment' => $assignment,
                'inputs' => $inputs->map(function ($item, $index) {
                    $item->setAttribute('index', $index + 1);
                    $item->setRelation('brand', $item->brand);
                    return $item;
                }),
            ],
        ]);

        return Inertia::render('Dealer/Index', [
            'can' => [
                'create.dealer' => Auth::user()->can('create.dealer'),
                'edit.dealer' => Auth::user()->can('edit.dealer'),
                'delete.dealer' => Auth::user()->can('delete.dealer'),
            ],
            'inputs' => $inputs->map(function ($item, $index) {
                return [
                    'id' => $item->id,
                    'index' => $index + 1,
                    'name' => $item->name,
                    'status' => $item->status,
                    'active' => $item->active,
                    'last_modified_at' => ($item->updated_at ? $item->updated_at : $item->created_at)->format('d-m-Y H:i'),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                ];
            }),
            'page_filters' => $pageFilters,
            'page_sort' =>  $pageSort,
            'page_limit' => (int) $pageLimit,
            'page_offset' => (int) $pageOffset,
            'total_records' => (int) $totalRecords,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($assignmentId)
    {
        // return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $assignmentId)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($assignmentId, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($assignmentId, $id)
    {
        // $assignment = Assignment::findOrFail($id);

        // return Inertia::render('Assignment/Edit', [
        //     'assignment' => $assignment
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $assignmentId, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($assignmentId, $id)
    {

    }
}
