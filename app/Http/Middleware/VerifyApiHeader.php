<?php

namespace App\Http\Middleware;

use Closure;

class VerifyApiHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->wantsJson() && $request->acceptsJson()) {
            return response()->json([
                'status' => 'fail',
                'code' => 'bad_request',
                'message' => 'Missing Headers',
            ], 400);
        }

        if (! $request->headers->get('X-IM-API') == 2) {
            return response()->json([
                'status' => 'fail',
                'code' => 'forbidden',
                'message' => 'Missing Headers',
            ], 403);
        }

        return $next($request);
    }
}
