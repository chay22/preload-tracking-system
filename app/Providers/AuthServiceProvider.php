<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\{
    Auth,
    Gate,
    Schema
};
use App\Models\Role;
use App\Models\Permission;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(null, [
            'prefix' => 'oauth',
            'namespace' => '\App\Http\Controllers\Passport',
        ]);

        Passport::tokensExpireIn(now()->addYears(2));

        Passport::refreshTokensExpireIn(now()->addYears(3));

        Passport::personalAccessTokensExpireIn(now()->addYears(6));

        if (Schema::hasTable('permissions')) {
            Permission::getCached()->each(function ($permission) {
                Gate::define($permission->key, function ($user) use ($permission) {
                    if (! $user) {
                        return false;
                    }

                    $role = $user->role;

                    if (! $role) {
                        return false;
                    }

                    return $user->hasPermission($permission->key);
                });
            });
        }
    }
}
