<?php

namespace App\Providers;

use Carbon\CarbonImmutable;
use Inertia\Inertia;
use League\Glide\Server;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;
use App\Models\Monitor;
use Illuminate\Support\Facades\{
    App,
    Auth,
    Config,
    Date,
    Schema,
    Session,
    Storage
};
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Pagination\UrlWindow;
use Illuminate\Support\Facades\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInertia();

        $this->registerJavascriptSharedData();

        $this->registerGlide();

        $this->registerLengthAwarePaginator();
    }

    protected function registerInertia()
    {
        // If you're using Laravel Mix, you can
        // use the mix-manifest.json for this.
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });
    }

    protected function registerJavascriptSharedData()
    {
        if (! Schema::hasTable('permissions')) {
             return;
        }

        Inertia::share([
            'app' => [
                'name' => Config::get('app.name')
            ],


            'auth' => function () {
                return [
                    'user' => Auth::user() ? [
                        'id' => Auth::user()->id,
                        'name' => Auth::user()->name,
                        'lgn' => Auth::user()->lgn,
                        'jns' => Auth::user()->jns,
                        'nik' => Auth::user()->nik,
                    ] : null
                ];
            },
            'url' => [
                'base' => url(''),
                'previous' => function () {
                    return url()->previous();
                }
            ],
            'csrfToken' => function () {
                return csrf_token();
            },
            'flash' => function () {
                $success = Session::get('success');

                if ($success && ! is_array($success)) {
                    $success = ['title' => 'Success', 'message' => $success];
                }

                $error = Session::get('error');

                if ($error && ! is_array($error)) {
                    $error = ['title' => 'Oops!', 'message' => $error];
                }

                return [
                    'success' => $success,
                    'error' => $error,
                ];
            },
            'errors' => function () {
                return Session::get('errors')
                    ? Session::get('errors')->getBag('default')->getMessages()
                    : (object) [];
            },
            'menu_access' => function () {
                if (Auth::check()) {
                    return array_values(array_filter(Auth::user()->getCachedAllowedPermissions(), function ($permission) {
                        return Str::startsWith($permission, 'access');
                    }));
                }

                return [];
            },
        ]);
    }

    protected function registerGlide()
    {
        $this->app->bind(Server::class, function ($app) {
            return Server::create([
                'source' => Storage::getDriver(),
                'cache' => Storage::getDriver(),
                'cache_folder' => '.glide-cache',
                'base_url' => 'img',
            ]);
        });
    }


    protected function registerLengthAwarePaginator()
    {
        $this->app->bind(LengthAwarePaginator::class, function ($app, $values) {
            return new class(...array_values($values)) extends LengthAwarePaginator {
                public function only(...$attributes)
                {
                    return $this->transform(function ($item) use ($attributes) {
                        return $item->only($attributes);
                    });
                }

                public function transform($callback)
                {
                    $this->items->transform($callback);

                    return $this;
                }

                public function toArray()
                {
                    $data = $this->items->toArray();

                    return [
                        'data' => $data,
                        'pagination' => [
                            'total' => $this->total(),
                            'count' => count($data),
                            'per_page' => $this->perPage(),
                            'current_page' => $this->currentPage(),
                            // 'from' => $this->firstItem(),
                            'total_pages' => $this->lastPage(),
                            // 'path' => $this->path(),
                            // 'to' => $this->lastItem(),
                            'links' => [
                                'previous' => $this->previousPageUrl(),
                                'next' => $this->nextPageUrl(),
                                'first' => $this->url(1),
                                'last' => $this->url($this->lastPage()),
                            ]
                        ]
                    ];

                    return [
                        'data' => $this->items->toArray(),
                        'links' => $this->links(),
                    ];
                }

                /**
                 * Get the URL for a given page number.
                 *
                 * @param  int  $page
                 * @return string
                 */
                public function url($page)
                {
                    $this->query = array_merge(Request::query(), $this->query);

                    return parent::url($page);
                }

                public function links($view = null, $data = [])
                {
                    return [

                    ];

                    $this->appends(Request::all());

                    $window = UrlWindow::make($this);

                    $elements = array_filter([
                        $window['first'],
                        is_array($window['slider']) ? '...' : null,
                        $window['slider'],
                        is_array($window['last']) ? '...' : null,
                        $window['last'],
                    ]);

                    return Collection::make($elements)->flatMap(function ($item) {
                        if (is_array($item)) {
                            return Collection::make($item)->map(function ($url, $page) {
                                return [
                                    'url' => $url,
                                    'label' => $page,
                                    'active' => $this->currentPage() === $page,
                                ];
                            });
                        } else {
                            return [
                                [
                                    'url' => null,
                                    'label' => '...',
                                    'active' => false,
                                ],
                            ];
                        }
                    })->prepend([
                        'url' => $this->previousPageUrl(),
                        'label' => 'Previous',
                        'active' => false,
                    ])->push([
                        'url' => $this->nextPageUrl(),
                        'label' => 'Next',
                        'active' => false,
                    ]);
                }
            };
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootSchemaDatabase();

        $this->bootCarbon();
    }

    protected function bootSchemaDatabase()
    {
        Schema::defaultStringLength(191);
    }

    protected function bootCarbon()
    {
        Date::use(CarbonImmutable::class);
    }
}
