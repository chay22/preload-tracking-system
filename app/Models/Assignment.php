<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use Concerns\ForAssignee, SoftDeletes;

    protected $table = 'assignments';

    protected $fillable = [
        'assigned_at', 'tool_id', 'dealer_id', 'status',
        'created_by', 'updated_by'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $dates = [
        'assigned_at',
    ];

    protected $casts = [
        'assigned_at' => 'date:Y-m-d'
    ];

    protected static $availableStatuses = [
        0 => 'draft',
        1 => 'done',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($instance) {
            if ($instance->isForceDeleting()) {
                $instance->assignees()->delete();
            }
        });

        // static::retrieved(function ($instance) {
        //     $owner = $instance->owner()->first();

        //     if ($owner) {
        //         $owner = $owner->user;
        //     }

        //     $instance->setRelation('owner', $owner);
        // });
    }

    public function dealer()
    {
        return $this->belongsTo(Dealer::class, 'dealer_id', 'id');
    }

    public function assignees()
    {
        return $this->hasMany(Assignee::class, 'assignment_id', 'id')->orderByDesc('is_owner');
    }

    public function inputs()
    {
        return $this->hasMany(AssignmentInput::class, 'assignment_id', 'id');
    }

    public function photos()
    {
        return $this->hasMany(AssignmentPhoto::class, 'assignment_id', 'id');
    }

    public function assistants()
    {
        return $this->belongsToMany(
            User::class,
            'assignment_assistant',
            'assignment_id',
            'user_id',
            'id',
            'id',
            'assistants'
        );
    }

    public function getUsersAttribute()
    {
        $user = $this->owner;

        if (! $user) {
            return [];
        }

        $user->attributes['type'] = 'owner';
        $users = [$user];

        foreach ($this->getRelation('assistants') as $assistant) {
            $assistant->attributes['type'] = 'assistant';
            $users[] = $assistant;
        }

        $userCount = count($users);

        $totalCount = $this->inputs->sum('success_count');
        $this->attributes['total_count'] = $totalCount;
        $count = round($totalCount / $userCount, 2, PHP_ROUND_HALF_DOWN);

        foreach ($users as $user) {
            $user->attributes['count'] = $count;
            $user->attributes['total_count'] = $totalCount;
        }

        return $users;
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function tool()
    {
        return $this->belongsTo(Tool::class, 'tool_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public static function getAvailableStatuses()
    {
        return static::$availableStatuses;
    }

    public static function getAvailableStatusNames()
    {
        $result = [];

        foreach (static::getAvailableStatuses() as $id => $status) {
            if ($status === 'draft') {
                $result[$id] = __('Draft');
            } else if ($status === 'done') {
                $result[$id] = __('Done');
            }
        }

        return $result;
    }
}
