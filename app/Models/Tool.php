<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tool extends Model
{
    use Concerns\HasActiveInactiveStatus, SoftDeletes;

    protected $table = 'tools';

    protected $fillable = [
        'id', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public $incrementing = false;
}
