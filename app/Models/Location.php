<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use Concerns\HasActiveInactiveStatus, SoftDeletes;

    protected $table = 'locations';

    protected $fillable = [
        'name', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'pivot', 'key',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($instance) {
            $instance->users()->detach([$instance->id]);
            $instance->dealers()->detach([$instance->id]);
        });
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function dealers()
    {
        return $this->belongsToMany(Dealer::class);
    }
}
