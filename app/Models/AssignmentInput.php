<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentInput extends Model
{
    use Concerns\ForAssignee;

    protected $table = 'assignment_inputs';

    protected $fillable = [
        'assignment_id', 'brand_type_id', 'success_count', 'failed_count',
        'created_by', 'updated_by',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by',
    ];

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'assignment_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(BrandType::class, 'brand_type_id', 'id');
    }

    public function brand()
    {
        return $this->type->brand;
    }

    public function getBrandAttribute()
    {
        return $this->brand();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
