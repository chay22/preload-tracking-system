<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dealer extends Model
{
    use Concerns\HasActiveInactiveStatus, Concerns\HasLocation, SoftDeletes;

    protected $table = 'dealers';

    protected $fillable = [
        'name', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
