<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Session;
use App\Exceptions\UnallowedAuthWithNoGroupException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Concerns\HasActiveInactiveStatus, Concerns\HasLocation, HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'role_id', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that are appended
     *
     * @var array
     */
    protected $appends = [
        'display_name',
    ];

    /**
     * Related role
     *
     * @return \Illuminate\Database\Eloquent\Relation\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function dealers()
    {
        return $this->belongsToMany(Dealer::class);
    }

    public function getDisplayNameAttribute()
    {
        if (isset($this->attributes['name']) && ! empty($this->attributes['name'])) {
            return $this->attributes['name'];
        }

        return $this->username;
    }

    /**
     * Get cached permission keys as array
     *
     * @return array
     */
    public function getCachedAllowedPermissions()
    {
        if (! Session::has('authpermissions')) {
            Session::put('authpermissions', $this->getAllowedPermissions());
        }

        return Session::get('authpermissions') ?: [];
    }

    /**
     * Get cached permission keys as array
     *
     * @return array
     */
    public function getAllowedPermissions()
    {
        $role = $this->role;

        if ($role) {
            return (array) $role->permissions()->get(['key'])->pluck('key')->toArray();
        }

        return [];
    }

    public function hasPermission($permission)
    {
        $availablePermissions = $this->getCachedAllowedPermissions();

        if (! $availablePermissions || ! count($availablePermissions)) {
            return false;
        }

        return in_array($permission, $availablePermissions);
    }

    public function findAndValidateForPassport($username, $password)
    {
        $user = static::where('username', $username)->first();

        if (Hash::check($password, $user->password)) {
            return $user;
        }

        throw ValidationException::withMessages([
            'username' => [trans('auth.failed')],
        ]);
    }
}
