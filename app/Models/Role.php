<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use Concerns\HasActiveInactiveStatus, SoftDeletes;

    protected $table = 'roles';

    protected $fillable = [
        'name', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($instance) {
            if ($instance->isForceDeleting()) {
                $instance->permissions()->delete();
            }
        });
    }

    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }

    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class,
            'role_permission',
            'role_id',
            'permission_id',
            'id',
            'id'
        );
    }
}
