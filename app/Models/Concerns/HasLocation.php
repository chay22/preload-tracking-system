<?php

namespace App\Models\Concerns;

use App\Models\Location;

trait HasLocation
{
    public function locations()
    {
        return $this->belongsToMany(Location::class);
    }
}
