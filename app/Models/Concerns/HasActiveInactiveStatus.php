<?php

namespace App\Models\Concerns;

trait HasActiveInactiveStatus
{
    public function setActiveAttribute($value)
    {
        $this->attributes['status'] = $value ? 1 : 0;
    }

    public function getActiveAttribute()
    {
        $status = $this->attributes['status'] ?? false;

        return (bool) $status;
    }
}
