<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Symfony\Component\Mime\MimeTypes;

class AssignmentPhoto extends Model
{
    use Concerns\ForAssignee;

    protected $table = 'assignment_photos';

    protected $fillable = [
        'assignment_id', 'path', 'extension', 'mime',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $appends = [
        'url',
    ];

    protected static $uploadDir = 'assignments';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($instance) {
            if (! $instance->id) {
                $instance->id = Str::uuid();
            }
        });

        static::deleting(function ($instance) {
            $instance->removeFile();
        });
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'assignment_id', 'id');
    }

    public function getPath()
    {
        return Storage::disk('public')->getAdapter()->getPathPrefix().$this->path;
    }

    public function getRelativePath()
    {
        return $this->path;
    }

    public function removeFile()
    {
        $path = $this->getRelativePath();

        if (Storage::disk('public')->exists($path)) {
            Storage::disk('public')->delete($path);
        }

        return $this;
    }

    public function uploadString($file)
    {
        $file = base64_decode($file);
        $extension = 'jpg';

        $finfo = finfo_open();
        $this->mime = $mime_type = finfo_buffer($finfo, $file, FILEINFO_MIME_TYPE);
        finfo_close($finfo);

        foreach (MimeTypes::getDefault()->getExtensions($mime_type) as $ext) {
            $extension = $ext;
            break;
        }

        $this->extension = $extension;

        $path = static::$uploadDir.DIRECTORY_SEPARATOR.str_replace('-', '', Str::uuid()).date('U').'.'.$extension;
        $fullpath = Storage::disk('public')->getAdapter()->getPathPrefix().$path;

        file_put_contents($fullpath, $file);

        $this->path = $path;

        return $this;
    }

    public function uploadFile(UploadedFile $file)
    {
        $this->extension = $extension = $file->getClientOriginalExtension();
        $this->mime = $mime = $file->getMimeType();
        $name = $file->getClientOriginalName();

        $path = str_replace('-', '', Str::uuid()).date('U').'.'.$extension;

        $this->path = $path = Storage::disk('public')->putFileAs(
            static::$uploadDir, $file, $path
        );

        return $this;
    }

    public static function upload(Assignment $assignment, $file)
    {
        $instance = (new static(['assignment_id' => $assignment->id]))->{(is_string($file) ? 'uploadString' : 'uploadFile')}($file);

        $instance->save();

        return $instance;
    }

    public function getUrlAttribute()
    {
        if (isset($this->attributes['path'])) {
            return Storage::disk('public')->url($this->getRelativePath());
        }
    }
}
