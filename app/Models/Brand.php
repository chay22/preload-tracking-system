<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use Concerns\HasActiveInactiveStatus, SoftDeletes;

    protected $table = 'brands';

    protected $fillable = [
        'name', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function types()
    {
        return $this->hasMany(BrandType::class, 'brand_id', 'id');
    }
}
