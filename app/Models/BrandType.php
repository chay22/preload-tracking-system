<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandType extends Model
{
    use Concerns\HasActiveInactiveStatus, SoftDeletes;

    protected $table = 'brand_types';

    protected $fillable = [
        'name', 'status', 'brand_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }
}
