<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Models\Assignment;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AssignmentsExport implements FromArray, ShouldAutoSize, WithStrictNullComparison, WithColumnFormatting, WithHeadings, WithEvents
{
    protected $assignments;

    protected $dealerToMerge = [];
    protected $brandToMerge = [];
    protected $result = [];

    public function __construct($assignments)
    {
        $this->assignments = $assignments;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $assignment = $this->assignments;

        $defaultData = [
            'no' => null,
            'assigned_at' => null,
            'dealer' => null,
            'tool' => null,
            'brand' => null,
            'type' => null,
            'success_count' => null,
            'failed_count' => null,
            'owner' => null,
            'assistants' => null,
        ];

        $result = [];
        $i = 1;

        foreach ($this->assignments as $assignment) {
            foreach ($assignment->inputs as $input) {
                $result[] = [
                    'no' => $i,
                    'assigned_at' => $assignment->assigned_at->format('d/m/Y'),
                    'dealer' => $assignment->dealer ? $assignment->dealer->name : '',
                    'tool' => $assignment->tool ? $assignment->tool->id : '',
                    'brand' => $input->type && $input->type->brand ? $input->type->brand->name : '',
                    'type' => $input->type ? $input->type->name : '',
                    'success_count' => $input->success_count,
                    'failed_count' => $input->failed_count,
                    'owner' => $assignment->owner ? $assignment->owner->display_name : '',
                    'assistants' => $input->assistants,
                ];

                $i++;
            }
        }

        $this->result = $result;

        return $result;
    }

    public function map($assignment): array
    {
        return [
            // $assignment['assigned_at'] ? Date::dateTimeToExcel($assignment['assigned_at']) : null,
            // $assignment['dealer'],
            $assignment['no'],
            $assignment['brand'],
            $assignment['type'],
            $assignment['success_count'],
            $assignment['failed_count'],
            // $assignment['assignee'],
        ];
    }

    public function headings(): array
    {
        return [
            ['No', 'Tanggal', 'Dealer', 'Tool', 'Brand', 'Tipe', 'Success', 'Failed', 'Leader', 'Asisten'],
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $sheet = $event->sheet;
                $event->sheet->getDelegate()->getStyle('A1:J1')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('A1:J1')->getAlignment()->setHorizontal('center');
            }
        ];
    }
}
