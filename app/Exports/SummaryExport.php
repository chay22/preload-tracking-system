<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Models\Assignment;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class SummaryExport implements FromArray, ShouldAutoSize, WithStrictNullComparison, WithColumnFormatting, WithHeadings, WithEvents
{
    protected $assignments;
    protected $users;

    protected $dealerToMerge = [];
    protected $brandToMerge = [];
    protected $result = [];

    public function __construct($assignments, $users)
    {
        $this->assignments = $assignments;
        ksort($users);
        $this->users = array_values($users);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $assignments = $this->assignments;
        $users = $this->users;

        $defaultData = [
            'no' => null,
            'assigned_at' => null,
            'dealer' => null,
            'tool' => null,
            'brand' => null,
            'type' => null,
            'success_count' => null,
            'failed_count' => null,
            'owner' => null,
            'assistants' => null,
        ];

        $result = [];
        $i = 1;

        foreach ($this->assignments as $assignment) {
            $result[] = [
                'no' => $i,
                'user' => $assignment['display_name'],
                'count' => $assignment['count'],
                'dealer' => $assignment['dealer']->name,
                'tool' => $assignment['tool']->id,
                'assigned_at' => Carbon::parse($assignment['assigned_at'])->format('d/m/Y'),
            ];

            $i++;
        }

        $this->result = $result;

        return $result;
    }

    public function map($assignment): array
    {
        return [
            // $assignment['assigned_at'] ? Date::dateTimeToExcel($assignment['assigned_at']) : null,
            // $assignment['dealer'],
            $assignment['no'],
            $assignment['user'],
            $assignment['count'],
            $assignment['dealer'],
            $assignment['tool'],
            $assignment['assigned_at'],
            // $assignment['assignee'],
        ];
    }

    public function headings(): array
    {
        return [
            ['No', 'User', 'Count', 'Dealer', 'Tool', 'Tanggal'],
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $sheet = $event->sheet;
                $sheet->getDelegate()->getStyle('A1:F1')->getFont()->setBold(true);
                $sheet->getDelegate()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');

                $totalSheet = (count($this->assignments) + 3);

                $sheet->setCellValue('A'.$totalSheet, 'No');
                $sheet->setCellValue('B'.$totalSheet, 'User');
                $sheet->setCellValue('C'.$totalSheet, 'Total');
                $sheet->getDelegate()->getStyle('A'.($totalSheet).':F'.($totalSheet))->getFont()->setBold(true);
                $sheet->getDelegate()->getStyle('A'.($totalSheet).':F'.($totalSheet))->getAlignment()->setHorizontal('center');

                foreach ($this->users as $key => $value) {
                    $currentSheet = $key + $totalSheet + 1;
                    $sheet->setCellValue('A'.$currentSheet, $key + 1);
                    $sheet->setCellValue('B'.$currentSheet, $value['display_name']);
                    $sheet->setCellValue('C'.$currentSheet, $value['total']);
                }
            }
        ];
    }
}
