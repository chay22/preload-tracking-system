<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Models\Assignment;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AssignmentExport implements FromArray, WithStrictNullComparison, WithMapping, WithHeadings, WithEvents
{
    protected $assignment;

    protected $dealerToMerge = [];
    protected $brandToMerge = [];

    public function __construct(Assignment $assignment)
    {
        $this->assignment = $assignment;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $assignment = $this->assignment;

        $defaultData = [
            'no' => null,
            'assigned_at' => null,
            'dealer' => null,
            'tool' => null,
            'brand' => null,
            'type' => null,
            'success_count' => null,
            'failed_count' => null,
            'owner' => null,
            'assistants' => null,
        ];

        $result = [];
        $prevDealer = null;
        $prevBrand = null;

        foreach ($assignment->inputs as $i => $input) {
            $index = $i + 1;

            $assigned_at = $assignment->assigned_at->toDateTime();
            $dealer = $assignment->dealer->name;
            $brand = $input->brand->name;
            $type = $input->type->name;
            $success_count = $input->success_count;
            $failed_count = $input->failed_count;

            $assignee = [];

            foreach ([
                $input->createdBy,
                $input->updatedBy,
            ] as $user) {
                if ($user && (! count($assignee) || ! in_array($user->display_name, $assignee))) {
                    $assignee[] = $user->display_name;
                }
            }

            $assignee = implode(', ', $assignee);

            if ($prevDealer == $dealer) {
                $this->dealerToMerge[$dealer] = [$this->dealerToMerge[$dealer][0], $index];
                $assigned_at = null;
                $dealer = null;
            } else {
                $this->dealerToMerge[$dealer] = [$index, null];
            }

            if ($prevBrand == $brand) {
                $this->brandToMerge[$brand] = [$this->brandToMerge[$brand][0], $index];
                $brand = null;
            } else {
                $this->brandToMerge[$brand] = [$index, null];
            }


            $assigned_at = $assignment->assigned_at->toDateTime();
            $dealer = $assignment->dealer->name;
            $brand = $input->brand->name;
            $type = $input->type->name;
            $success_count = $input->success_count;
            $failed_count = $input->failed_count;

            if (isset($result[$i])) {
                $result[$i]['no'] = $index;
                $result[$i]['assigned_at'] = $assigned_at;
                $result[$i]['dealer'] = $dealer;
                $result[$i]['brand'] = $brand;
                $result[$i]['type'] = $type;
                $result[$i]['success_count'] = $success_count;
                $result[$i]['failed_count'] = $failed_count;
                $result[$i]['assignee'] = $assignee;
            } else {
                $result[] = [
                    'no' => $index,
                    'assigned_at' => $assigned_at,
                    'dealer' => $dealer,
                    'brand' => $brand,
                    'type' => $type,
                    'success_count' => $success_count,
                    'failed_count' => $failed_count,
                    'assignee' => $assignee,
                ];
            }

            $prevDealer = $dealer ? $dealer : $prevDealer;
            $prevBrand = $brand ? $brand : $prevBrand;
        }

        return $result;

        // $assignees = implode(', ', $assignees);

        $i = 2;
        $prevAssignedAt = null;
        $prevBrand = null;
        $prevCellToMergeStart = null;
        $prevCellToMergeEnd = null;

        foreach ($assignment->inputs as $no => $input) {
            $data = ['no' => $no + 1];

            if ($prevDealer = $assignment->dealer->name) {
                $data['assigned_at'] = null;
                $data['dealer'] = null;
            } else {
                $data['assigned_at'] = $assignment->assigned_at->toDateTime();
                $data['dealer'] = $assignment->dealer->name;
            }

            if ($prevBrand == $input->brand->name) {
                $data['brand'] = null;

                $prevCellToMergeEnd = $i;
            } else {
                $data['brand'] = $input->brand->name;

                if ($prevCellToMergeStart && $prevCellToMergeEnd) {
                    $this->cellToMerge[] = [$prevCellToMergeStart, $prevCellToMergeEnd];
                    $prevCellToMergeEnd = null;
                }

                $prevCellToMergeStart = $i;

            }

            $data['brand_type'] = $input->type->name;

            $data['success_count'] = $input->success_count;
            $data['failed_count'] = $input->failed_count;

            if ($i === 2) {
                $data['assignees'] = $assignees;
            } else {
                $data['assignees'] = null;
            }

            $result[] = $data;

            $i++;
        }

        return $result;
    }

    public function map($assignment): array
    {
        return [
            // $assignment['assigned_at'] ? Date::dateTimeToExcel($assignment['assigned_at']) : null,
            // $assignment['dealer'],
            $assignment['no'],
            $assignment['brand'],
            $assignment['type'],
            $assignment['success_count'],
            $assignment['failed_count'],
            // $assignment['assignee'],
        ];
    }

    public function drawings($sheet)
    {
        $totalInput = $this->assignment->inputs->count();
        $totalInput += 8;

        $drawings = [];

        $i = 0;

        foreach ($this->assignment->photos as $photo) {
            $drawing = new Drawing();
            $drawing->setName('photo');
            $drawing->setPath($photo->getPath());
            $drawing->setHeight(500);

            if ($i === 0) {
                $drawing->setCoordinates('A'.$totalInput);
                $drawing->setWorksheet($sheet->getDelegate());
            } else {
                $drawing->setCoordinates('D'.$totalInput);
                $drawing->setWorksheet($sheet->getDelegate());
                break;
            }


            $drawings[] = $drawing;

            $i++;
        }

        return $drawings;
    }

    public function headings(): array
    {
        return [
            ['Assign date', '', $this->assignment->assigned_at->format('d/m/Y')],
            ['Tool ID', '', $this->assignment->tool ? $this->assignment->tool->id : ''],
            ['Dealer', '', $this->assignment->dealer ? $this->assignment->dealer->name : ''],
            ['Assistants', '', $this->assignment->assistants],
            [''],
            ['No', 'Brand', 'Tipe', 'Success', 'Failed'],
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $sheet = $event->sheet;

                $sheet->getDelegate()->getStyle('C1')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                $sheet->mergeCells('A1:B1');
                $sheet->mergeCells('A2:B2');
                $sheet->mergeCells('A3:B3');
                $sheet->mergeCells('A4:B4');
                $sheet->mergeCells('C1:E1');
                $sheet->mergeCells('C2:E2');
                $sheet->mergeCells('C3:E3');
                $sheet->mergeCells('C4:E4');

                $totalInput = $this->assignment->inputs->count();

                $sheet->getDelegate()->getStyle('A7:A'.($totalInput + 7))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
                $sheet->getDelegate()->getStyle('D7:D'.($totalInput + 7))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
                $sheet->getDelegate()->getStyle('E7:E'.($totalInput + 7))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);


                $event->sheet->getDelegate()->getStyle('A6:E6')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('A6:E6')->getAlignment()->setHorizontal('center');

                $event->sheet->getColumnDimension('A')->setWidth(20);
                $event->sheet->getColumnDimension('B')->setWidth(20);
                $event->sheet->getColumnDimension('C')->setWidth(20);
                $event->sheet->getColumnDimension('D')->setWidth(20);
                $event->sheet->getColumnDimension('E')->setWidth(20);

                $this->drawings($sheet);
            }
        ];
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

                foreach ($this->dealerToMerge as $merge) {
                    $from = $merge[0];
                    $to = $merge[1];

                    if ($from && $to) {
                        $from = $merge[0] + 1;
                        $to = $merge[1] + 1;

                        $cellRange = 'A'.$from.':A'.$to;
                        $event->sheet->mergeCells($cellRange);
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal('left');
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical('top');

                        $cellRange = 'B'.$from.':B'.$to;
                        $event->sheet->mergeCells($cellRange);
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal('left');
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical('top');
                    }

                }

                foreach ($this->brandToMerge as $merge) {
                    $from = $merge[0];
                    $to = $merge[1];

                    if ($from && $to) {
                        $from = $merge[0] + 1;
                        $to = $merge[1] + 1;

                        $cellRange = 'C'.$from.':C'.$to;
                        $event->sheet->mergeCells($cellRange);
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal('left');
                        $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical('top');
                    }
                }
            },
        ];
    }
}
