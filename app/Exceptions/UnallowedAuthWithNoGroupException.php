<?php

namespace App\Exceptions;

use Exception;

class UnallowedAuthWithNoGroupException extends Exception
{
    public function __construct($message = 'Grup tidak ditemukan. Mohon hubungi admin', $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
