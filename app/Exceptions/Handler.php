<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Support\Facades\Session;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Throwable $exception)
    {
        $response = parent::render($request, $exception);

        if ((App::environment('production')
            || $exception instanceof InvalidUserCredentialException
            || $exception instanceof UnallowedAuthWithNoGroupException)
            && $request->header('X-Inertia')
            && in_array($response->status(), [500, 503, 404, 403, 422])
        ) {
            $statusCode = $response->status();

            if ($exception instanceof InvalidUserCredentialException) {
                Session::flash('error', 'Username atau password Anda tidak sesuai.');
                $statusCode = 422;

                return redirect()->back();
            } else if ($exception instanceof UnallowedAuthWithNoGroupException) {
                Session::flash('error', 'Grup tidak ditemukan. Mohon hubungi admin');
                $statusCode = 422;

                return redirect()->back();
            }

            return Inertia::render('Error', ['status' => $response->status()])
                ->toResponse($request)
                ->setStatusCode($statusCode);
        }

        return $response;
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        $errors = $exception->errors();
        $message = $exception->getMessage();

        foreach ($errors as $error) {
            if (is_array($error) && isset($error[0])) {
                $message = $error[0];
            }

            break;
        }

        return response()->json([
            'status' => 'fail',
            'code' => 'validation',
            'message' => $message,
            'errors' => $errors,
        ], $exception->status);
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  \Exception  $e
     * @return array
     */
    protected function convertExceptionToArray(Throwable $e)
    {
        $status = 'error';
        $code = 'server_error';
        $message = 'Server Error';

        if ($this->isHttpException($e)) {
            $statusCode = $e->getStatusCode();
            $statusTexts = SymfonyResponse::$statusTexts;

            if ($statusCode >= 400 && $statusCode < 500) {
                $status = 'fail';
                $code = 'error';

                if ($statusCode == 400) {
                    $code = 'bad_request';
                    $message = 'Bad Request';
                } else if ($statusCode == 401) {
                    $code = 'unauthorized';
                    $message = 'Unauthorized';
                } else if ($statusCode == 403) {
                    $code = 'forbidden';
                    $message = 'Forbidden';
                } else if ($statusCode == 422) {
                    $code = 'validation';
                    $message = ' Unprocessable Entity';
                } else if (isset($statusTexts[$statusCode])) {
                    $message = $statusTexts[$statusCode];
                    $code = Str::snake($message);
                }
            } else if (isset($statusTexts[$statusCode])) {
                $message = $statusTexts[$statusCode];
                $code = Str::snake($message);
            }
        }

        return config('app.debug') ? [
            'status' => $this->isHttpException($e) && $e->getStatusCode() >= 400 && $e->getStatusCode() < 500 ? 'fail' : 'error',
            'code' => $code,
            'message' => $e->getMessage(),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all(),
        ] : [
            'status' => $this->isHttpException($e) && $e->getStatusCode() >= 400 && $e->getStatusCode() < 500 ? 'fail' : 'error',
            'code' => $code,
            'message' => $this->isHttpException($e) && $e->getMessage() ? $e->getMessage() : $message,
        ];
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
                    ? response()->json([
                        'status' => 'fail',
                        'code' => 'unauthenticated',
                        'message' => trans('auth.failed'),
                    ], 401)
                    : redirect()->guest($exception->redirectTo() ?? route('login'));
    }
}
