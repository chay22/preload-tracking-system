export default {
  beforeCreate() {
    const html = document.querySelector('html')

    let name = this.name

    if (!name) {
      name = this.$options.name
    }

    html && name && html.classList.add(name)
  }
}
