export default {
  props: {
    info_text: Array,
  },
  mounted() {
    this.$nextTick(() => {
      this.$parent.setInfoText(this.info_text || [])
      this.onPlayMarkerClicked()
    })
  },
  data() {
    return {
      isMediaPlaying: false,
    }
  },
  methods: {
    onPlayMarkerClicked() {
      this.$parent.onMediaPlay()
      this.isMediaPlaying = true
    },
  }
}
