import Vue from 'vue'
import axios from 'axios'
import VueMeta from 'vue-meta'
import PortalVue from 'portal-vue'
import { InertiaApp } from '@inertiajs/inertia-vue'
import NProgress from 'nprogress'
import 'dayjs/locale/id'
import dayjsPluginAdvancedFormat from 'dayjs/plugin/advancedFormat'
import dayjsPluginLocaleData from 'dayjs/plugin/localeData'
import dayjsPluginLocalizedFormat from 'dayjs/plugin/localizedFormat'
import dayjsPluginToObject from 'dayjs/plugin/toObject'
import dayjsPluginIsSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import dayjsPluginIsSameOrBefore from 'dayjs/plugin/isSameOrBefore'
import dayjsPluginCustomParseFormat from 'dayjs/plugin/customParseFormat'
import dayjsPluginRelativeTime from 'dayjs/plugin/relativeTime'
import dayjs from 'dayjs'
import stringToColor from 'string-to-color'

import secureRandomString from 'secure-random-string'

import AutoComplete from 'primevue/autocomplete'
import BlockUI from 'primevue/blockui';
import Calendar from 'primevue/calendar'
import Checkbox from 'primevue/checkbox'
import Chips from 'primevue/chips'
// import ColorPicker from 'primevue/colorpicker'
import Dropdown from 'primevue/dropdown'
// import Editor from 'primevue/editor'
import InputMask from 'primevue/inputmask'
import InputSwitch from 'primevue/inputswitch'
import InputText from 'primevue/inputtext'
import Listbox from 'primevue/listbox'
import MultiSelect from 'primevue/multiselect'
import Password from 'primevue/password'
import RadioButton from 'primevue/radiobutton'
import Rating from 'primevue/rating'
import SelectButton from 'primevue/selectbutton'
import Slider from 'primevue/slider'
// import Spinner from 'primevue/spinner'
import Textarea from 'primevue/textarea'
import ToggleButton from 'primevue/togglebutton'
import TriStateCheckbox from 'primevue/tristatecheckbox'
import Button from 'primevue/button'
import SplitButton from 'primevue/splitbutton'
import Carousel from 'primevue/carousel'
import DataTable from 'primevue/datatable'
import Column from 'primevue/column'
import ColumnGroup from 'primevue/columngroup'
import DataView from 'primevue/dataview'
import DataViewLayoutOptions from 'primevue/dataviewlayoutoptions'
// import FullCalendar from 'primevue/fullcalendar'
import OrderList from 'primevue/orderlist'
// import OrganizationChart from 'primevue/organizationchart'
import Paginator from 'primevue/paginator'
// import PickList from 'primevue/picklist'
import Tree from 'primevue/tree'
import TreeTable from 'primevue/treetable'
import Accordion from 'primevue/accordion'
import AccordionTab from 'primevue/accordiontab'
import Card from 'primevue/card'
import DeferredContent from 'primevue/deferredcontent'
import Fieldset from 'primevue/fieldset'
import Panel from 'primevue/panel'
import TabView from 'primevue/tabview'
import TabPanel from 'primevue/tabpanel'
import Toolbar from 'primevue/toolbar'
import Dialog from 'primevue/dialog'
import OverlayPanel from 'primevue/overlaypanel'
import Sidebar from 'primevue/sidebar'
import Tooltip from 'primevue/tooltip'
import FileUpload from 'primevue/fileupload'
// import Breadcrumb from 'primevue/breadcrumb'
// import ContextMenu from 'primevue/contextmenu'
// import MegaMenu from 'primevue/megamenu'
import Menu from 'primevue/menu'
import Menubar from 'primevue/menubar'
import PanelMenu from 'primevue/panelmenu'
import Steps from 'primevue/steps'
import TabMenu from 'primevue/tabmenu'
import TieredMenu from 'primevue/tieredmenu'
import Chart from 'primevue/chart'
import Message from 'primevue/message'
import ToastService from 'primevue/toastservice'
import Toast from 'primevue/toast'
import Inplace from 'primevue/inplace'
import ProgressBar from 'primevue/progressbar'
import ProgressSpinner from 'primevue/progressspinner'

// import vGanttChart from 'v-gantt-chart';
import VueTimepicker from 'vue2-timepicker'
import storage from 'qiao.ls.js'
/**
 * Vue
 */
window.Vue = global.Vue = Vue
Vue.config.productionTip = false

/**
 * Inertia.js
 */
Vue.use(InertiaApp)

/**
 * Vue Meta
 */
Vue.use(VueMeta)

/**
 * PortalVue
 */
Vue.use(PortalVue)

/**
 * Ziggy routes
 */
Vue.prototype.$route = (...args) => route(...args).url()
Vue.mixin({ methods: { route: window.route } })

/**
 * NProgress
 */
window.NProgress = global.NProgress = NProgress

/**
 * dayjs
 */
window.dayjs = global.dayjs = dayjs
dayjs.extend(dayjsPluginAdvancedFormat)
dayjs.extend(dayjsPluginLocaleData)
dayjs.extend(dayjsPluginLocalizedFormat)
dayjs.extend(dayjsPluginToObject)
dayjs.extend(dayjsPluginIsSameOrAfter)
dayjs.extend(dayjsPluginIsSameOrBefore)
dayjs.extend(dayjsPluginCustomParseFormat)
dayjs.extend(dayjsPluginRelativeTime)
dayjs.Ls.en.weekStart = 1
dayjs.Ls.id.weekStart = 1
// dayjs.locale('id')

/**
 * Primevue
 */
Vue.use(ToastService)
Vue.directive('tooltip', Tooltip)
Vue.component('AutoComplete', AutoComplete)
Vue.component('BlockUI', BlockUI)
Vue.component('Calendar', Calendar)
Vue.component('Checkbox', Checkbox)
Vue.component('Chips', Chips)
// Vue.component('ColorPicker', ColorPicker)
Vue.component('Dropdown', Dropdown)
Vue.component('InputMask', InputMask)
Vue.component('InputSwitch', InputSwitch)
Vue.component('InputText', InputText)
Vue.component('Listbox', Listbox)
Vue.component('MultiSelect', MultiSelect)
Vue.component('Password', Password)
Vue.component('RadioButton', RadioButton)
Vue.component('Rating', Rating)
Vue.component('SelectButton', SelectButton)
Vue.component('Slider', Slider)
// Vue.component('Spinner', Spinner)
Vue.component('Textarea', Textarea)
Vue.component('ToggleButton', ToggleButton)
Vue.component('TriStateCheckbox', TriStateCheckbox)
Vue.component('Button', Button)
Vue.component('SplitButton', SplitButton)
Vue.component('Carousel', Carousel)
Vue.component('DataTable', DataTable)
Vue.component('Column', Column)
Vue.component('ColumnGroup', ColumnGroup)
Vue.component('DataView', DataView)
Vue.component('DataViewLayoutOptions', DataViewLayoutOptions)
// Vue.component('FullCalendar', FullCalendar)
Vue.component('OrderList', OrderList)
// Vue.component('OrganizationChart', OrganizationChart)
Vue.component('Paginator', Paginator)
// Vue.component('PickList', PickList)
Vue.component('Tree', Tree)
Vue.component('TreeTable', TreeTable)
Vue.component('Accordion', Accordion)
Vue.component('AccordionTab', AccordionTab)
Vue.component('Card', Card)
Vue.component('DeferredContent', DeferredContent)
Vue.component('Fieldset', Fieldset)
Vue.component('Panel', Panel)
Vue.component('TabView', TabView)
Vue.component('TabPanel', TabPanel)
Vue.component('Toolbar', Toolbar)
Vue.component('Dialog', Dialog)
Vue.component('OverlayPanel', OverlayPanel)
Vue.component('Sidebar', Sidebar)
Vue.component('FileUpload', FileUpload)
// Vue.component('Breadcrumb', Breadcrumb)
// Vue.component('ContextMenu', ContextMenu)
// Vue.component('MegaMenu', MegaMenu)
Vue.component('Menu', Menu)
Vue.component('Menubar', Menubar)
Vue.component('PanelMenu', PanelMenu)
Vue.component('Steps', Steps)
Vue.component('TabMenu', TabMenu)
Vue.component('TieredMenu', TieredMenu)
Vue.component('Chart', Chart)
Vue.component('Message', Message)
Vue.component('Toast', Toast)
Vue.component('Inplace', Inplace)
Vue.component('ProgressBar', ProgressBar)
Vue.component('ProgressSpinner', ProgressSpinner)

Vue.component('vue-timepicker', VueTimepicker)
/**
 * Vue Gantt Chart
 */
// Vue.use(vGanttChart)

/**
 * String to color
 */
window.stringToColor = stringToColor

/**
 * Axios
 */
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['X-CSRF-TOKEN'] = ''

window.axios = global.axios = axios

/**
 * LocalStorage with expiriration time
 */
window.storage = storage

/**
 * Util
 */
window.rsdkUtil = global.rsdkUtil = {
  random: (v = 10, fn = null) => {
    if (typeof v === 'number') {
      v = { length: v }
    }

    if (!v.length) {
      v.length = 10
    }

    return secureRandomString(v, fn)
  },
  arrChunk: (arr, len) => {
    var chunks = [],
        i = 0,
        n = arr.length;

    while (i < n) {
      chunks.push(arr.slice(i, i += len));
    }

    return chunks;
  }
}

/**
 * Init!
 */
let app = document.getElementById('app')

window.Temadata = new Vue({
  metaInfo: {
    titleTemplate: (title) => title ? `${title} - Preload Tracking System` : 'Preload Tracking System'
  },
  methods: {
    confirmDelete(message) {
      window.Temadata.$emit('global-confirm-delete:open', message)

      return new Promise((resolve) => {
        window.Temadata.$once('global-confirm-delete:close', (confirmed) => {
          this.$nextTick(() => {
            resolve(confirmed)
          })
        })
      })
    }
  },
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => require(`./Pages/${name}`).default,
    },
  })
}).$mount(app)
