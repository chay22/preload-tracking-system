const mix = require('laravel-mix')
require('laravel-mix-polyfill');
const path = require('path')
const fs = require('fs')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const staticDir = fs.readdirSync(path.resolve(__dirname, 'resources', 'static'))

for (let i = 0; i < staticDir.length; i++) {
  mix.copyDirectory(`resources/static/${staticDir[i]}`, `public/${staticDir[i]}`)
}
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
      output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
      resolve: {
        alias: {
          vue$: 'vue/dist/vue.runtime.esm.js',
          '@': path.resolve('resources/js'),
        },
      },
    })
    .version()
    .sourceMaps()
    .polyfill({
        enabled: true,
        useBuiltIns: 'usage',
        targets: {
          firefox: '50'
        }
     })
