<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api', 'as' => 'api.'], function () {
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/', 'Api\UserController@index')->name('index');
        Route::get('/me', 'Api\UserController@me')->name('me');
    });

    Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
        Route::get('/', 'Api\RoleController@index')->name('index');
    });

    Route::group(['prefix' => 'tool', 'as' => 'tool.'], function () {
        Route::get('/', 'Api\ToolController@index')->name('index');
    });

    Route::group(['prefix' => 'dealer', 'as' => 'dealer.'], function () {
        Route::get('/', 'Api\DealerController@index')->name('index');
    });

    Route::group(['prefix' => 'summary', 'as' => 'summary.'], function () {
        Route::get('/brand', 'Api\SummaryController@brand')->name('brand');
    });

    Route::group(['prefix' => 'assignment', 'as' => 'assignment.'], function () {
        Route::get('/', 'Api\AssignmentController@index')->name('index');
        Route::post('/', 'Api\AssignmentController@store')->name('store');

        Route::group(['prefix' => '{assignmentId}/assignee', 'as' => 'assignee.'], function () {
            Route::get('/', 'Api\AssignmentAssigneeController@index')->name('index');
            Route::post('/', 'Api\AssignmentAssigneeController@store')->name('store');
            Route::patch('/{id}', 'Api\AssignmentAssigneeController@update')->name('update');
            Route::delete('/{id}', 'Api\AssignmentAssigneeController@destroy')->name('destroy');
        });

        Route::group(['prefix' => '{assignmentId}/input', 'as' => 'input.'], function () {
            Route::get('/', 'Api\AssignmentInputController@index')->name('index');
            Route::post('/', 'Api\AssignmentInputController@store')->name('store');
            Route::patch('/{id}', 'Api\AssignmentInputController@update')->name('update');
            Route::delete('/{id}', 'Api\AssignmentInputController@destroy')->name('destroy');
        });

        Route::group(['prefix' => '{assignmentId}/photo', 'as' => 'photo.'], function () {
            Route::get('/', 'Api\AssignmentPhotoController@index')->name('index');
            Route::post('/', 'Api\AssignmentPhotoController@store')->name('store');
            Route::patch('/{id}', 'Api\AssignmentPhotoController@update')->name('update');
            Route::delete('/{id}', 'Api\AssignmentPhotoController@destroy')->name('destroy');
        });

        Route::post('/{id}/done', 'Api\AssignmentController@done')->name('done');
        Route::patch('/{id}', 'Api\AssignmentController@update')->name('update');
        Route::delete('/{id}', 'Api\AssignmentController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'brand', 'as' => 'brand.'], function () {
        Route::get('/', 'Api\BrandController@index')->name('index');

        Route::group(['prefix' => '{brandId}/type', 'as' => 'type.'], function () {
            Route::get('/', 'Api\BrandTypeController@index')->name('index');
        });
    });
});
