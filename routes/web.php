<?php

use Illuminate\Support\Facades\{
    Auth,
    Route
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/get-assignment-last-7-day', 'DashboardController@getAssignmentLast7Days')->name('dashboard.assignment-last-7-days-data');
    Route::get('/dashboard/get-assignment-by-assignee', 'DashboardController@getAssignmentByAssignees')->name('dashboard.assignment-by-assignee-data');

    Route::get('users/me', 'UsersController@me')->name('users.me');
    Route::resource('users', 'UsersController');
    Route::group(['prefix' => 'role-permission'], function () {
        Route::get('{id}', 'RolePermissionController@edit')->name('role-permission.edit');
        Route::patch('{id}', 'RolePermissionController@update')->name('role-permission.update');
    });
    Route::resource('role', 'RoleController');
    Route::resource('tool', 'ToolController');
    Route::resource('dealer', 'DealerController');
    Route::resource('location', 'LocationController');

    Route::group(['prefix' => 'brand/{brandId}', 'as' => 'brand-type.'], function () {
        Route::get('/', 'BrandTypeController@index')->name('index');
        Route::post('/', 'BrandTypeController@store')->name('store');
        Route::patch('/{id}', 'BrandTypeController@update')->name('update');
        Route::delete('/{id}', 'BrandTypeController@destroy')->name('destroy');
    });
    Route::resource('brand', 'BrandController');
    Route::group(['prefix' => 'assignment/{assignmentId}/input', 'as' => 'assignment-input.'], function () {
        Route::get('/data', 'AssignmentInputController@data')->name('data');
    });
    Route::group(['prefix' => 'assignment/{assignmentId}/photo', 'as' => 'assignment-photo.'], function () {
        Route::get('/data', 'AssignmentPhotoController@data')->name('data');
    });

    Route::post('assignment/export', 'AssignmentController@export')->name('assignment.export');
    Route::resource('assignment', 'AssignmentController');

    Route::group(['prefix' => 'report'], function () {
        Route::post('summary/export', 'SummaryController@export')->name('report.summary.export');
        Route::get('summary', 'SummaryController@index')->name('report.summary.index');
    });
});
